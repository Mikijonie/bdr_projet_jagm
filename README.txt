Pr�requis : un serveur local MySQL ayant un acc�s root (username : root, password : root), ainsi que Java install�
(Pour changer ces acc�s dans l'application, atteindre le constructeur de JagmBDD, dans le fichier src/jagm/JagmBDD.java) et recompiler.

1) Ex�cuter le fichier SQL_Projet.sql sur le serveur MySQL apr�s s'�tre connect� (par exemple, avec Workbench). Si une base de donn�es au nom de JAGM existe, elle sera supprim�e.
2) Ex�cuter le fichier SQL_Projet_Triggers.sql
3) Ex�cuter le fichier SQL_Projet_Views.sql
4) Ex�cuter le fichier SQL_Projet_Insertions.sql

Le fichier SQL_Projet_RequestsFonctionnalities.sql n'a �t� utilis� que dans le cadre du d�veloppement de l'application

5) Acc�der au dossier dist depuis un terminal, et lancer l'application compil�e avec "java -jar BDR_JAGM.jar".

Un terminal d'une largeur suffisante est n�cessaire pour afficher la totalit� des colonnes.

Pour la connexion, un utilisateur membre a �t� ajout� : 
	username : user		mdp : user
Pour la connexion, un utilisateur admin a �t� ajout� : 
	username : root		mdp : root