/*
    PROJET JAGM - Application
    JANVIER 2017
    Nathan Gonzalez Montes, Rémi Jacquemard, Aurélie Levy, Mika Pagani
 */
package jagm;

import jagm.pages.*;
import java.sql.SQLException;
import java.util.Stack;

/**
 *
 * @author Rémi
 */
public class Jagm {

    Page currentPage = new MainPage();
    Stack<Page> lastPages = new Stack<>();

    public Jagm() {
        lastPages.add(currentPage);
        try {
            while (lastPages.size() > 0) {
                //Affiche la page actuel
                //La page suivante est retournée par show()
                Page nextPage = currentPage.show();
                if (nextPage == null) { //On remonte
                    this.currentPage = lastPages.pop();
                } else if (nextPage != currentPage) {
                    lastPages.add(this.currentPage);
                    this.currentPage = nextPage;
                }

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Jagm();
    }

}
