/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm;

import java.util.LinkedList;

public class StringFormatHelper {    
    /**
     * Le nombre de colonne doit être égale entre rows et titles
     * @param titles
     * @param rows
     * @return 
     */
    public static String getFormatedString(String[] titles, String[][] rows){
        if(rows.length > 0 && titles.length != rows[0].length)
            throw new IllegalArgumentException("Le nombre de colonne doit être égale entre rows et titles !");
        
        //On parcours une fois pour connaitre la taille idéal des colonnes
        int colWidths[] = new int[titles.length];
        for(int i = 0; i < titles.length; i++) //Pour chaque colonne du titre
            colWidths[i] = titles[i].length();
        
        for(int row = 0; row < rows.length; row++){ //Pour chaque ligne
            for(int col = 0; col < rows[row].length; col++){ //Pour chaque colonne
                if(rows[row][col].length() > colWidths[col])
                    colWidths[col] = rows[row][col].length();
            }
        }      
        
        //---- MISE EN PAGE
        String lineFormat = "";
        int tableWidth = 0;
        
        for(int i = 0; i < titles.length; i++){ //Pour chaque colonne
            lineFormat += "%" + (colWidths[i] + 1) + "s│";
            tableWidth += colWidths[i] + 2;
        }
        
        String str = "";
        str += String.format(lineFormat + "\n", (Object[])titles);
        
        for(int i = 0; i < tableWidth; i++)
            str += "═";
        str += "\n";
        
        for(int i = 0; i < rows.length; i++){ //Pour chaque ligne
            str += String.format(lineFormat, (Object[]) rows[i]);
            
            if(i < rows.length - 1) //Pas de \n en fin du tableau
                str += "\n";
        }
        
        return str;
    }
    
    /**
     * Le nombre de colonne doit être égale entre rows et titles
     * @param titles
     * @param rows
     * @return 
     */
    public static String getFormatedString(String[] titles, LinkedList<String[]> rows){
        return StringFormatHelper.getFormatedString(titles, rows.toArray(new String[rows.size()][titles.length]));
    }
    

}
