/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Nathan
 */
class ReviewPage extends Page {
    @Override
    public Page show() throws SQLException {
        String[] titles = new String[]{
            "Note", "Commentaire", "Titre du jeu"};
        
        LinkedList<String[]> records = db.getRecords("liste_avis", "Note", "Commentaire", "JeuTitre_fk");
        
        System.out.println(StringFormatHelper.getFormatedString(titles, records));
        return null;
    }
    
}
