/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Rémi
 */
public class AddPenaltyPage extends Page {

    public AddPenaltyPage() {
    }

    @Override
    public Page show() throws SQLException {
        // EMPRUNTS
        System.out.println("Emprunt en cours : ");

        String[] titles = new String[]{
            "N°", "Pseudo", "Jeu", "Console"};

        LinkedList<String[]> records = db.getRecords("liste_emprunts", "Id", "Pseudo", "NomJeu_fk", "NomConsole_fk");

        System.out.println(StringFormatHelper.getFormatedString(titles, records));

        System.out.print("Entrer le numéro de l'emprunt : ");
        int emprunt = getInt();

        if (!db.exists("liste_emprunts", "Id", emprunt)) {
            System.err.println("L'emprunt indiquée n'existe pas");
            return null;
        }

        // Type amendes
        titles = new String[]{
            "Nom", "Prix Journée", "Montant unique"};
        records = db.getRecords("TypeAmende", "Nom", "PrixJournee", "MontantUnique");
        System.out.println(StringFormatHelper.getFormatedString(titles, records));

        System.out.print("Entrer le nom de l'amende : ");
        String type = sc.nextLine();

        if (!db.exists("TypeAmende", "Nom", type)) {
            System.err.println(type + " n'existe pas");
            return null;
        }

        db.createAmende(emprunt, type );
        System.out.println("L'amende a bien été créé");

        return null;
    }

}
