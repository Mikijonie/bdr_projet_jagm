/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Rémi
 */
public abstract class MenuPage extends Page {

    public class Action {

        private String name;
        private Page p;

        public Action(String name, Page p) {
            this(name);
            this.p = p;
        }

        public Action(String name) {
            this.name = name;
        }
    }

    //Les pages peuvent être nulles
    protected final LinkedList<Action> actions = new LinkedList<>();

    private int choice;
    private Page nextPage;
    
    public MenuPage(){
        actions.add(new Action("Retour", null));
    }

    protected void showMenu() {
        int i = 0;
        for (Action page : actions) {
            System.out.println(i + " - " + page.name);
            i++;
        }
        System.out.print("> ");

        try {
            choice = getInt();
            nextPage = actions.get(choice).p;            
        } catch (IndexOutOfBoundsException e) {
            //Par défaut, on revient sur la page courante
            nextPage = this;
        }
    }

    protected Page getNextPage() {
        if (choice == -1) {
            System.exit(0); //On quitte
        }
        return nextPage;
    }

    protected int getChoice() {
        return choice;
    }

    @Override
    public Page show() throws SQLException {
        showMenu();
        return getNextPage();
    }

}
