/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class EtatPage extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println(StringFormatHelper.getFormatedString(new String[]{
            "Titre de l'etat",}, db.getRecords("Etat", "Titre")));
        return null;
    }
}
