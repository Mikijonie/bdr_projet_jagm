package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Mika Pagani
 */
public class ExtendLoanPage extends Page {

    public ExtendLoanPage() {
    }
    
    @Override
    public Page show() throws SQLException {
        String[] titles = new String[]{
            "N°","Pseudo", "Jeu", "Console", "Date de l'emprunt", "Date de rendu maximale", "Prolongations effectuée", "Lieu", "Id"
        };

        LinkedList<String[]> records = db.getRecords("liste_emprunts_date",
                "Pseudo",                
                "NomJeu_fk",
                "NomConsole_fk",
                "DateEmprunt",
                "date_fin",
                "NombreProlongation",
                "NPA_fk",
                "Id"
                );
        LinkedList<String[]> nLoan = new LinkedList<>();
        //Ajout du numéro devant
        for (int i = 0; i < records.size(); i++) {
            String[] game = new String[records.get(i).length + 1];
            game[0] = "" + (i + 1);
            for (int j = 1; j < game.length; j++) {
                game[j] = records.get(i)[j - 1];
            }
            nLoan.add(game);
        }

        System.out.println(StringFormatHelper.getFormatedString(titles, nLoan));
        
        System.out.print("Quel jeu voulez vous prolonger ? ");
        int choiceGame = getInt();
        
        if (choiceGame >= 1 && choiceGame <= records.size()) {
            String s = records.get(choiceGame - 1)[7];
            
            int idChoice = Integer.parseInt(s);
            
            if(db.extendLoan(idChoice)){
                System.out.println("La prolongation s'est déroulée avec succès ! ");
            } else {
                System.err.println("Erreur lors de la prolongation de l'emprunt...");
            }
            return null;
        }
        
        return null;
    }
    
}
