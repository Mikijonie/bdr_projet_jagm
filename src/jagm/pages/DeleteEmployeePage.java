/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author Rémi
 */
public class DeleteEmployeePage extends Page {

    @Override
    public Page show() throws SQLException {
        //Demande---
        System.out.print("Entrez le pseudo de l'employé à supprimer : ");

        String pseudo = sc.nextLine();

        if (db.deleteEmployee(pseudo)) {
            System.out.println("La suppression s'est déroulée avec succès ! ");
        } else {
            System.err.println("Erreur lors de la supression de l'employé...");
        }

        return null;
    }

}
