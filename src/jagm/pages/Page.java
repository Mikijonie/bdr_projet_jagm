/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.JagmBDD;
import java.sql.SQLException;
import java.util.Scanner;

/**
 *
 * @author Rémi
 */
public abstract class Page {

    protected JagmBDD db;
    protected Scanner sc = new Scanner(System.in);

    public Page() {
        try {
            this.db = JagmBDD.getInstance();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * *
     * Doit retourner: - null : on revient à la page précédente - this : on
     * reste sur la page courante - une nouvelle page sinon
     *
     * @return La page suivante à montrer
     */
    public abstract Page show() throws SQLException;

    /**
     * Demande un entier à l'utilisateur
     *
     * @return
     */
    protected int getInt() {
        while (true) {
            try {
                String str = sc.nextLine();
                return Integer.parseInt(str);
            } catch (NumberFormatException ex) {
                System.out.print("Entrez un entier : ");
            }

        }
    }

    /**
     * Demande un double à l'utilisateur
     *
     * @return
     */
    protected double getDouble() {
        while (true) {
            try {
                String str = sc.nextLine();
                return Double.parseDouble(str);
            } catch (NumberFormatException ex) {
                System.out.print("Entrez un nombre à virgule : ");
            }

        }
    }
}
