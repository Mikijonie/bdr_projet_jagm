/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class InsertSampleConsole extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println("Insertion exemplaire de console");
        /*Id int(10) NOT NULL AUTO_INCREMENT,
    NumExemplaire int(10) NOT NULL,
	EtatCommentaire varchar(100),
    TitreEtat_fk varchar(10),
    NPA_fk int(4) NOT NULL,
    NomConsole_fk varchar(20),
    NomJeu_fk varchar(50),*/

        // ------ DEMANDES
        System.out.print("Nom de la console: ");
        String nomConsole = sc.nextLine();
        System.out.print("Numero de l'exemplaire : ");
        int numEx = getInt();
        System.out.print("Titre etat de la console: ");
        String titreEtat = sc.nextLine();
        System.out.print("Commentaire etat de la console: ");
        String EtatComment = sc.nextLine();

        System.out.print("NPA du magasin ou est la console: ");
        int NPA = getInt();

        if (db.insertSampleConsole(numEx, EtatComment, titreEtat, nomConsole, NPA)) {
            System.out.println("Insertion de l'exemplaire numero " + numEx + " de la console " + nomConsole + " reussie");
            return null;
        }
        return null;
    }

}
