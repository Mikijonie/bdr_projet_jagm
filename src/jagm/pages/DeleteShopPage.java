/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class DeleteShopPage extends Page{

    @Override
    public Page show() throws SQLException {
        System.out.println("Suppression magasin");

        // ------ DEMANDES
         System.out.print("Ville du magasin a supprimer: ");
        String ville = sc.nextLine();
        System.out.print("Rue : ");
        String rue = sc.nextLine();
        

        if (db.deleteShop(ville, rue)) {
            System.out.println("Suppression du magasin situe à " + ville + " reussie");
            return null;
        }
        
        System.out.println("Erreur, réessayez l'effacement.");
        return new DeleteShopPage();
    }
}
