/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Mika Pagani
 */
public class DeleteLoanPage extends Page{
    
    DeleteLoanPage(){}
    
    @Override
    public Page show() throws SQLException {
        
        LinkedList<String[]> records;
        
        String[] titles = new String[]{
            "N°", "Pseudo", "Jeu", "Console", "Date de l'emprunt", "Date de rendu maximale", "Prolongations effectuée", "Lieu", "Id"
        };

        records = db.getRecords("liste_emprunts_date", "Pseudo",                
                "NomJeu_fk",
                "NomConsole_fk",
                "DateEmprunt",
                "date_fin",
                "NombreProlongation",
                "NPA_fk",
                "Id"
                );

        LinkedList<String[]> nEmprunts = new LinkedList<>();
        //Ajout du numéro devant
        for (int i = 0; i < records.size(); i++) {
            String[] game = new String[records.get(i).length + 1];
            game[0] = "" + (i + 1);
            for (int j = 1; j < game.length; j++) {
                game[j] = records.get(i)[j - 1];
            }
            nEmprunts.add(game);
        }

        System.out.println(StringFormatHelper.getFormatedString(titles, nEmprunts));

        System.out.print("Quel emprunt souhaitez vous supprimer ? ");
        int choice = getInt();

        if(choice >= 1 && choice <= records.size()) {
            String s = records.get(choice-1)[7];

            int idChoice = Integer.parseInt(s);
            
            if(db.deleteLoan(idChoice) == 0){
                System.out.println("La suppression s'est déroulée avec succès ! ");
            } else {
                System.out.println("Erreur lors de la supression de l'emprunt...");
            }
        }
        return null;
    }
}
