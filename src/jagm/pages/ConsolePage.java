/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;

/**
 *
 * @author Rémi
 */
public class ConsolePage extends Page {

    @Override
    public Page show() throws SQLException {
        //System.out.println("Nombre d'exemplaires disponibles\tTitre de la Console");
        /*
        for (Console c : db.getFreeConsole()) {
            System.out.println(c.nbr_ex_consoles + "\t" + c.titreConsole);
        }*/

 /*
        for (String[] cols : db.getRecords("consoles_disponibles", "nbr_ex_consoles", "NomConsole_fk")) {
            for (String col : cols) {
                System.out.print(col + "\t");
            }
            System.out.println("");
        }
         */
        System.out.println(StringFormatHelper.getFormatedString(new String[]{
            "Nombre d'exemplaires disponibles",
            "Titre de la Console"},
            db.getRecords("consoles_disponibles", "nbr_ex_consoles", "NomConsole_fk")
        ));
        
        return null;
    }

}
