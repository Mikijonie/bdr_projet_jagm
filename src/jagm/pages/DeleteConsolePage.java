/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.SQLException;

/**
 *
 * @author aurel
 */


public class DeleteConsolePage extends Page {

    public DeleteConsolePage() {
    }

    @Override
    public Page show() throws SQLException {
        System.out.println("Suppression console");

        // ------ DEMANDES
        System.out.print("Nom : ");
        String nom = sc.nextLine();

        try {
            if (!db.exists("Console", "Nom", nom)) {
                System.err.println("La console " + nom + " n'existe pas");
            } else {
                db.deleteConsole(nom);
                System.out.println("Suppression de " + nom + " reussie");
            }
        } catch (MySQLIntegrityConstraintViolationException ex) {
            System.err.println("Des jeux utilises encore cette console !");
        }

        return null;
    }
}
