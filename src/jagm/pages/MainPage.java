/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

/**
 *
 * @author Rémi
 */
public class MainPage extends MenuPage {

    public MainPage() {
        actions.clear();
        actions.add(new Action("Quitter"));
        actions.add(new Action("Se connecter", new ConnectionPage()));   
        actions.add(new Action("S'inscrire", new RegistrationPage()));
    }

}
