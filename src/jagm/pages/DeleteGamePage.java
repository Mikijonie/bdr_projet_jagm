/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class DeleteGamePage extends Page {

    public DeleteGamePage() {
    }

    @Override
    public Page show() throws SQLException {
        System.out.println("Suppression jeu");

        // ------ DEMANDES
        System.out.print("Titre du jeu a supprimer: ");
        String titre = sc.nextLine();

        try {
            if (!db.exists("Jeu", "Titre", titre)) {
                System.err.println("Le jeu " + titre + " n'existe pas");
            } else {
                db.deleteGame(titre);
                System.out.println("Suppression de " + titre + " reussie");
            }
        } catch (MySQLIntegrityConstraintViolationException ex) {
            System.err.println("Des emprunts utilisent encore ce jeu !");
        }

        return null;
    }
}
