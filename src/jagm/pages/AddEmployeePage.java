/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author Rémi
 */
public class AddEmployeePage extends Page {

    @Override
    public Page show() throws SQLException {
        // ------ DEMANDES
        System.out.print("Nom : ");
        String nom = sc.nextLine();

        System.out.print("Prenom : ");
        String prenom = sc.nextLine();

        System.out.print("Nom d'utilisateur : ");
        String user = sc.nextLine();

        System.out.print("Mot de passe : ");
        String password = sc.nextLine();

        System.out.print("Age : ");
        int age = getInt();

        System.out.print("NPA : ");
        int NPA = getInt();

        System.out.print("Rue : ");
        String rue = sc.nextLine();

        System.out.print("Ville : ");
        String ville = sc.nextLine();

        System.out.print("Email : ");
        String mail = sc.nextLine();

        System.out.print("Numéro de téléphone : ");
        String numTel = sc.nextLine();

        System.out.print("Salaire : ");
        double salary = getDouble();
        
        System.out.print("NPA du magasin : ");
        int NPAMagasin = getInt();

        System.out.println("");
        //-------------
        
        if(db.createEmployee(nom, prenom, user, password, age, NPA, rue, ville, mail, numTel, salary, NPAMagasin))
            System.out.println("Vous pouvez maintenant vous connecter !");
        else
            System.err.println("L'utilisateur existe déjà, ou le magasin n'existe pas !");

        return null;
        
    }

}
