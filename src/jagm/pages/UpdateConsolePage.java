/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class UpdateConsolePage extends Page {

    public UpdateConsolePage() {
    }

    @Override
    public Page show() throws SQLException {
        System.out.println("Modification console");

        // ------ DEMANDES
        System.out.print("Nom de la console a modifier: ");
        String nomModif = sc.nextLine();

        if (!db.exists("Console", "Nom", nomModif)) {
            System.err.println("La console " + nomModif + " n'existe pas");
            return null;
        }

        System.out.print("Nom : ");
        String nom = sc.nextLine();
        System.out.print("Annee production : ");
        int anneeInt = getInt();

        db.updateConsole(nom, anneeInt, nomModif);
        System.out.println("Modification de " + nom + " reussie. Anciennement " + nomModif);
        return null;

    }
}
