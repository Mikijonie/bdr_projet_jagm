/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;

/**
 *
 * @author Rémi
 */
public class GamePage extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println(StringFormatHelper.getFormatedString(new String[]{
            "Nombre d'exemplaires disponibles",
            "Titre du Jeu",
            "Titre de la Console"
        }, db.getRecords("jeux_disponibles", "nbr_ex_jeux", "NomJeu_fk", "NomConsole_fk")));

        /*
        System.out.println("Nombre d'exemplaires disponibles\tTitre du Jeu\tTitre de la Console");
        for (Game g : db.getFreeGames()) {
            System.out.println(g.nbr_ex_jeux + "\t" +g.titreJeu + "\t" + g.titreConsole);
        }*/
        return null;
    }

}

class ExemplaireGame extends GamePage {

    private int numExemplaire;
    private String etatCommentaire;

    public void insertExemplaire() {

    }
}
