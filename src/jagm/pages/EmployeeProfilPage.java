/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;

/**
 *
 * @author Nathan
 */
public class EmployeeProfilPage extends Page{
    
    @Override
    public Page show() throws SQLException {
        String[] titles = new String[]{
            "Nom", "Prénom", "Pseudo", "Age", "Rue", "Ville", "NPA", "Mail", "N°Téléphone", "Salaire"};
        
        String[][] records = new String[][]{
                db.getRecord("voir_profile_employe", "Pseudo", db.getUsername(), "Nom", "Prenom", "Pseudo", "Age",
                        "AdrRue", "AdrVille", "AdrNPA", "Mail", "NumTel", "Salaire")
        };
        
        System.out.println(StringFormatHelper.getFormatedString(titles, records));
        return null;
    }
    
}
