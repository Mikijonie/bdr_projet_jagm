/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Rémi
 */
public class AddConsoleReservationPage extends Page {

    @Override
    public Page show() throws SQLException {
        String[] titles = {"N°", "Console", "Nombre d'exemplaires"};
        LinkedList<String[]> consoles
                = db.getRecords("consoles_disponibles", "NomConsole_fk", "nbr_ex_consoles");

        LinkedList<String[]> nConsoles = new LinkedList<>();
        //Ajout du numéro devant
        for (int i = 0; i < consoles.size(); i++) {
            String[] console = new String[consoles.get(i).length + 1];
            console[0] = "" + (i + 1);
            for (int j = 1; j < console.length; j++) {
                console[j] = consoles.get(i)[j - 1];
            }
            nConsoles.add(console);
        }

        System.out.println(StringFormatHelper.getFormatedString(titles, nConsoles));

        System.out.print("Quel console voulez vous réserver ? ");
        int choice = getInt();

        if (choice >= 1 && choice <= consoles.size()) {
            String consoleChoice = consoles.get(choice - 1)[0];
            
            System.out.println(consoleChoice);

            db.insertReservation(null, consoleChoice, db.getUserId());
            return null;
        } else {
            return this;
        }
    }
    
}
