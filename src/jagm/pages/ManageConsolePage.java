/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

/**
 *
 * @author Rémi
 */
public class ManageConsolePage extends MenuPage {

    public ManageConsolePage() {
        actions.add(new Action("Inserer un exemplaire de console", new InsertSampleConsole()));
        actions.add(new Action("Modifier un exemplaire de console", new UpdateSampleConsole()));
        actions.add(new Action("Supprimer un exemplaire de console", new DeleteSampleConsole()));
    }

}
