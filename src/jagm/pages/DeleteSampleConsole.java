/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class DeleteSampleConsole extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println("Suppression exemplaire de console");

        // ------ DEMANDES
        System.out.print("Nom de la console: ");
        String nom = sc.nextLine();
        System.out.print("Numero de l'exemplaire : ");
        int num = getInt();

        try {
            if (!db.exists("exemplaire", "NomConsole_fk = '" + nom + "' and NumExemplaire = '" + num + "'")) {
                System.err.println("l'exemplaire numero " + num + " de la console + " + nom + " n'existe pas");
            } else {
                db.deleteSampleConsole(nom, num);
                System.out.println("Suppression de " + "l'exemplaire numero " + num + " de la console + " + nom + " reussie");
            }
        } catch (MySQLIntegrityConstraintViolationException ex) {
            System.err.println("suppression demandée impossible !");
        }
        return null;
    }
}
