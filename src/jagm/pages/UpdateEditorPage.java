/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class UpdateEditorPage extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println("Modification editeur");

        // ------ DEMANDES
        System.out.print("Nom de l'editeur a modifier: ");
        String nomOld = sc.nextLine();

        if (!db.exists("Editeur", "Nom", nomOld)) {
            System.err.println("L'editeur " + nomOld + " n'existe pas");
            return null;
        }

        System.out.print("Nouveau nom : ");
        String nomNew = sc.nextLine();

        db.updateEditeur(nomNew, nomOld);
        System.out.println("Modification de " + nomNew + " reussie. Anciennement " + nomOld);
        return null;
    }
}
