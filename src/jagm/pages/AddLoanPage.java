package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Rémi
 */
public class AddLoanPage extends Page {

    @Override
    public Page show() throws SQLException {
        String[] titles = {"N°", "Jeu", "Console", "Nombre d'exemplaires"};
        LinkedList<String[]> games
                = db.getRecords("jeux_disponibles", "NomJeu_fk", "NomConsole_fk", "nbr_ex_jeux");

        LinkedList<String[]> nGames = new LinkedList<>();
        //Ajout du numéro devant
        for (int i = 0; i < games.size(); i++) {
            String[] game = new String[games.get(i).length + 1];
            game[0] = "" + (i + 1);
            for (int j = 1; j < game.length; j++) {
                game[j] = games.get(i)[j - 1];
            }
            nGames.add(game);
        }

        System.out.println(StringFormatHelper.getFormatedString(titles, nGames));

        System.out.print("Quel jeu voulez vous emprunter ? ");
        int choiceGame = getInt();
        
        System.out.print("A qui l'emprunt est-il fait (id)? ");
        int choiceMember = getInt();

        if (choiceGame >= 1 && choiceGame <= games.size()) {
            String titleChoice = games.get(choiceGame - 1)[0];
            String consoleChoice = games.get(choiceGame - 1)[1];
            System.out.print("Combien de jours ? ");
            int numDay = getInt();

            db.insertLoan(choiceMember, titleChoice, consoleChoice, numDay);
            return null;
        } else {
            return this;
        }
    }
    
}
