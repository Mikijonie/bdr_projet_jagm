/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class UpdateSampleGame extends Page{

    @Override
    public Page show() throws SQLException {
        System.out.println("Modification d'un exemplaire de jeu");

        // ------ DEMANDES
        System.out.print("Nom du jeu a modifier: ");
        String nomJeu = sc.nextLine();
        if (!db.exists("Jeu", "Titre", nomJeu)) {
            System.err.println("Le jeu '" + nomJeu + "' n'existe pas");
            return null;
        }else{
            System.out.print("Numero de l'exemplaire a modifier: ");
            int numEx = getInt();
            String whereCondition = "NomJeu_fk = '" + nomJeu + "' AND NumExemplaire = '" + numEx + "'";
            if (!db.exists("Exemplaire", whereCondition)) {
                System.err.println("Le numéro '" + numEx + "' de l'exemplaire du jeu '" + nomJeu + "' n'existe pas");
                return null;
            }else{
                System.out.print("Nouveau commentaire etat du jeu: ");
                String etatCommentaire = sc.nextLine();
                
                System.out.print("Nouveau titre etat du jeu: ");
                String titreEtat = sc.nextLine();
            
                if (!db.exists("Etat", "Titre", titreEtat)) {
                    System.err.println("L'état du jeu '" + titreEtat + "' n'existe pas");
                    return null;
                }

                db.updateSampleGame(etatCommentaire, titreEtat, numEx, nomJeu);
                
                System.out.println("Modification de l'exemplaire num " + numEx + " du jeu " + nomJeu + " reussie.");
                return null;
            }
        }

    }
}
