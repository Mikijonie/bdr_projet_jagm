/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author Mika Pagani
 */
public class HandleLoanPage extends MenuPage {
    
    public HandleLoanPage() {
        actions.add(new Action("Lister tous les emprunts effectués", new AllLoanPage()));
        actions.add(new Action("Lister les emprunts d'un membre", new MemberLoanPage()));
        actions.add(new Action("Ajouter un emprunt", new AddLoanPage()));
        actions.add(new Action("Supprimer un emprunt", new DeleteLoanPage()));
        actions.add(new Action("Prolonger un emprunt", new ExtendLoanPage()));
    }
    
    @Override
    public Page show() throws SQLException {
        return super.show();
    }
}
