/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class InsertGamePage extends Page {

    public InsertGamePage() {
    }

    @Override
    public Page show() throws SQLException {
        System.out.println("Insertion jeu");

        // ------ DEMANDES
        System.out.print("Titre: ");
        String titre = sc.nextLine();

        if (db.exists("Jeu", "Titre", titre)) {
            System.err.println("Le jeu " + titre + " existe déjà");
            return null;
        }

        System.out.print("Annee de production: ");
//        String anneeProduction = sc.nextLine();
        int anneeProductionInt = getInt();
        System.out.print("Limite d'age: ");
//        String limiteAge = sc.nextLine();
        int limiteAgeInt = getInt();

        System.out.print("Nom de la console: ");
        String nomConsoleFk = sc.nextLine();
        if (!db.exists("Console", "Nom", nomConsoleFk)) {
            System.err.println("La console " + nomConsoleFk + " n'existe pas");
            return null;
        }

        System.out.print("nom de l'editeur: ");
        String nomEditeur = sc.nextLine();
        if (!db.exists("Editeur", "Nom", nomEditeur)) {
            System.err.println("L'editeur " + nomEditeur + " n'existe pas");
            return null;
        }

        System.out.print("Genre: ");
        String genre = sc.nextLine();

        if (!db.exists("Genre", "Titre", genre)) {
            System.err.println("Le genre " + genre + " n'existe pas");
            return null;
        }

        db.insertGame(titre, anneeProductionInt, limiteAgeInt, nomConsoleFk, nomEditeur, genre);
        System.out.println("Insertion de " + titre + " reussie");
        return null;

    }
}
