/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class UpdateGamePage extends Page {

    public UpdateGamePage() {
    }

    @Override
    public Page show() throws SQLException {
        System.out.println("Modification jeu");

        // ------ DEMANDES
        System.out.print("Titre du jeu a modifier: ");
        String titreAModif = sc.nextLine();

        if (!db.exists("Jeu", "Titre", titreAModif)) {
            System.err.println("Le jeu " + titreAModif + " n'existe pas");
            return null;
        }

        System.out.print("Titre: ");
        String Titre = sc.nextLine();
        System.out.print("Annee de production: ");
        int anneeProductionInt = getInt();
        System.out.print("Limite d'age: ");
        int limiteAgeInt = getInt();

        System.out.print("Nom de la console: ");
        String nomConsoleFk = sc.nextLine();

        if (!db.exists("Console", "Nom", nomConsoleFk)) {
            System.err.println("La console " + nomConsoleFk + " n'existe pas");
            return null;
        }

        System.out.print("nom de l'editeur: ");
        String nomEditeur = sc.nextLine();
        if (!db.exists("Editeur", "Nom", nomEditeur)) {
            System.err.println("L'editeur " + nomConsoleFk + " n'existe pas");
            return null;
        }

        System.out.print("Genre: ");
        String genre = sc.nextLine();
        if (!db.exists("Genre", "Titre", genre)) {
            System.err.println("Le genre " + nomConsoleFk + " n'existe pas");
            return null;
        }

        db.updateGame(Titre, anneeProductionInt, limiteAgeInt, nomConsoleFk, nomEditeur, genre, titreAModif);
        System.out.println("Modification de " + titreAModif + " reussie. Anciennement " + Titre);
        return null;
    }
}
