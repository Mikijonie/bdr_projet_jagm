/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Rémi
 */
public class HandlePenaltyPage extends MenuPage {

    public HandlePenaltyPage() {
        actions.add(new Action("Ajouter une amende", new AddPenaltyPage()));
        actions.add(new Action("Supprimer une amende"));
    }

    @Override
    public Page show() throws SQLException {

        System.out.println("Amendes en cours : ");

        String[] titles = new String[]{
            "N°", "Date", "Type", "Titre du jeu", "Pseudo", "Nombre de jour", "A payer"};

        LinkedList<String[]> records = db.getRecords("liste_amendes", "Amende_id", "DateInitial", "TypeAmende_fk", "NomJeu_fk", "Pseudo", "Jours", "PrixTotal");

        System.out.println(StringFormatHelper.getFormatedString(titles, records));

        //Affiche le menu
        super.show();

        if (super.getChoice() == 2 && records.size() >0) {
            System.out.print("Entrer le numéro de l'amende a supprimer : ");
            int choice = getInt();

            if (!db.exists("Amende", "Id", choice)) {
                System.err.println("L'amende indiquée n'existe pas");
            } else {
                db.deleteAmende(choice);
                System.out.println("L'amende a bien été supprimée");
            }

        }

        return super.getNextPage();
    }

}
