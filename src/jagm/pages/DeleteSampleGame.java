/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class DeleteSampleGame extends Page{

    @Override
    public Page show() throws SQLException {
        System.out.println("Suppression exemplaire du jeu");

        // ------ DEMANDES
        System.out.print("Nom du jeu: ");
        String nom = sc.nextLine();
        
        if (!db.exists("Jeu", "Titre", nom)) {
            System.err.println("Le jeu '" + nom + "' n'existe pas");
            return null;
        }
        
        System.out.print("Numero de l'exemplaire : ");
        int num = getInt();
        String whereCondition = "NomJeu_fk = '" + nom + "' AND NumExemplaire = '" + num + "'";
        if (!db.exists("Exemplaire", whereCondition)) {
            System.err.println("Le numéro '" + num + "' de l'exemplaire du jeu '" + nom + "' n'existe pas");
            return null;
        }
        
        try {
            db.deleteSampleGame(nom, num);
            System.out.println("Suppression de " + "l'exemplaire numero " + num + " du jeu " + nom + " reussie");
        } catch (MySQLIntegrityConstraintViolationException ex) {
            System.err.println("Suppression demandée impossible !");
        }
        return null;
    }
}
