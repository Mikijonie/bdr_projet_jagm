/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Rémi
 */
public class MemberReservationPage extends MenuPage {

    public MemberReservationPage(){
        actions.add(new Action("Ajouter une réservation de jeu", new AddGameReservationPage()));
        actions.add(new Action("Ajouter une réservation de console", new AddConsoleReservationPage()));
        actions.add(new Action("Annuler une réservation", new DeleteReservationPage()));
    }

    @Override
    public Page show() throws SQLException {
        System.out.println("Mes réservations : ");
         String[] titles = new String[]{
            "Jeu", "Console", "Date de retrait", "Lieu"
        };

        LinkedList<String[]> records = db.getRecordsWithFilter("liste_reservation", "Pseudo", db.getUsername(),                
                "NomJeu_fk",
                "NomConsole_fk",
                "DateRetraitSouhaite",
                "NPA_fk"
                );
        
        System.out.println(StringFormatHelper.getFormatedString(titles, records));
        
        super.showMenu();
        
        return getNextPage();
    }
    
}
