/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class InsertStatePage extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println("Insertion etat");

        // ------ DEMANDES
        System.out.print("Titre: ");
        String titre = sc.nextLine();
        if (db.exists("Etat", "Titre", titre)) {
            System.err.println("L'état " + titre + " existe déjà");
            return null;
        }

        db.insertState(titre);

        System.out.println("Insertion de " + titre + " reussie");
        return null;

    }
}
