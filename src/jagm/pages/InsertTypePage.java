/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class InsertTypePage extends Page {

    public InsertTypePage() {
    }

    @Override
    public Page show() throws SQLException {
        System.out.println("Insertion Genre");

        // ------ DEMANDES
        System.out.print("Titre: ");
        String titre = sc.nextLine();

        if (db.exists("Genre", "Titre", titre)) {
            System.err.println("Le genre" + titre + " existe déjà");
            return null;
        } else {
            db.insertGenre(titre);
            System.out.println("Insertion de " + titre + " reussie");
        }
        return null;
    }
}
