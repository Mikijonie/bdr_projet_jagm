/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;


/**
 *
 * @author aurel
 */
public class MagasinPage extends MenuPage{
     
    public MagasinPage() {
        actions.add(new MenuPage.Action("Afficher un magasin", new AfficherMagasin()));
        actions.add(new MenuPage.Action("Inserer un magasin", new InsertShopPage()));
        actions.add(new MenuPage.Action("Modifier un magasin", new UpdateShopPage()));
        actions.add(new MenuPage.Action("Supprimer un magasin", new DeleteShopPage()));
    }
}
