/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class UpdateStatePage extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println("Modification etat");

        // ------ DEMANDES
        System.out.print("Titre de l'etat a modifier: ");
        String titreOld = sc.nextLine();

        if (!db.exists("Etat", "Titre", titreOld)) {
            System.err.println("L'état " + titreOld + " n'existe pas");
            return null;
        }

        System.out.print("Nouveau titre : ");
        String titreNew = sc.nextLine();

        db.updateState(titreNew, titreOld);
        System.out.println("Modification de " + titreNew + " reussie. Anciennement " + titreOld);
        return null;

    }
}
