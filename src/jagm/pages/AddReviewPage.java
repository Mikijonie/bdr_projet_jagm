/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Rémi
 */
public class AddReviewPage extends Page {

    @Override
    public Page show() throws SQLException {
        String[] titles = new String[]{
            "N°", "Titre du jeu", "Editeur"};

        LinkedList<String[]> games = db.getRecords("Jeu", "Titre", "NomEditeur_fk");
        LinkedList<String[]> nGames = new LinkedList<>();

        //Ajout du numéro devant
        for (int i = 0; i < games.size(); i++) {
            String[] game = new String[games.get(i).length + 1];
            game[0] = "" + (i + 1);
            for (int j = 1; j < game.length; j++) {
                game[j] = games.get(i)[j - 1];
            }
            nGames.add(game);
        }

        System.out.println(StringFormatHelper.getFormatedString(titles, nGames));

        System.out.print("Entrez le N° du jeu auquel ajouter un avis : ");

        int choice = getInt();

        System.out.print("Note x/10 : ");
        int note = getInt();

        System.out.println("Commentaire (ENTER pour ne pas entrer de commentaire) : ");
        String comment = sc.nextLine();
        if (comment.equals("")) {
            comment = null;
        }

        if (note >= 1 && note <= 10 && choice >= 1 && choice <= games.size()) {
            String titleChoice = games.get(choice - 1)[0];

            db.insertReview(titleChoice, note, comment);
            return null;
        } else {
            return this;
        }
    }

}
