/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class DeleteEditorPage extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println("Suppression editeur");

        // ------ DEMANDES
        System.out.print("Nom : ");
        String nom = sc.nextLine();
        try {
            if (db.exists("Editeur", "Nom", nom)) {
                db.deleteEditeur(nom);
                System.out.println("Suppression de " + nom + " reussie");
            } else {
                System.err.println("L'editeur " + nom + " n'existe pas");
            }
        } catch (MySQLIntegrityConstraintViolationException ex) {
            System.err.println("Des jeux utilises encore cet editeur !");
        }
        
        return null;
    }

}
