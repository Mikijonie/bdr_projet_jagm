/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class DeleteStatePage extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println("Suppression etat");

        // ------ DEMANDES
        System.out.print("Titre : ");
        String titre = sc.nextLine();

        try {
            if (!db.exists("Etat", "Titre", titre)) {
                System.err.println(titre + " n'existe pas");
            } else {
                db.deleteEtat(titre);
                System.out.println("Suppression de " + titre + " reussie");
            }
        } catch (MySQLIntegrityConstraintViolationException ex) {
            System.err.println("Des jeux utilises encore cet état !");
        }
        return null;
    }
}
