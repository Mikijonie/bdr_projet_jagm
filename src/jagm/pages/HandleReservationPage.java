/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author Nathan
 */
class HandleReservationPage extends MenuPage {

    public HandleReservationPage() {
        actions.add(new Action("Lister toutes les réservations effectuées", new AllReservationPage()));
        actions.add(new Action("Lister les réservations d'un seul membre", new AllReservationPage()));
        actions.add(new Action("Supprimer une réservation", new DeleteReservationPage()));
    }

    @Override
    public Page show() throws SQLException {
        return super.show();
    }
    
}
