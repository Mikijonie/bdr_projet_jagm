package jagm.pages;
import java.sql.SQLException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author aurel
 */
public class UpdateShopPage extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println("Modification magasin");

        // ------ DEMANDES
        System.out.print("Ville du magasin a modifier: ");
        String ville = sc.nextLine();
        System.out.print("Ville du magasin (nouveau nom): ");
        String villenew = sc.nextLine();
        System.out.print("Ancienne rue : ");
        String rueold = sc.nextLine();
        System.out.print("Nouvelle rue : ");
        String rueNew = sc.nextLine();
        System.out.print("Numéro rue: ");
//        String annee = sc.nextLine();
        int numRue = getInt();
        System.out.print("NPA: ");
        int NPA = getInt();

//        int anneeInt = Integer.parseInt(annee);
        if (db.updateShop(villenew, rueNew, numRue, NPA, ville, rueold)) {
            System.out.println("Modification du magasin à " + ville + " reussie.");
            return null;
        }
        
        System.out.println("Erreur, réessayez l'actualisation.");
        return new UpdateShopPage();
    }
}
