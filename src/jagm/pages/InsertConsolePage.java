/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class InsertConsolePage extends Page {

    public InsertConsolePage() {
    }

    @Override
    public Page show() throws SQLException {
        System.out.println("Insertion console");

        // ------ DEMANDES
        System.out.print("Nom : ");
        String nom = sc.nextLine();
        
        if (db.exists("Console", "Nom", nom)) {
            System.err.println("La console " + nom + " existe déjà");
            return null;
        }

        System.out.print("Annee production : ");
        //String annee = sc.nextLine();
        int anneeInt = getInt();

        db.insertConsole(nom, anneeInt);

        System.out.println("Insertion de " + nom + " reussie");

        return null;
    }
}
