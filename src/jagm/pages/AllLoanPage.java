/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Rémi
 */
public class AllLoanPage extends Page {

    public AllLoanPage() {
    }

    @Override
    public Page show() throws SQLException {
        String[] titles = new String[]{
            "Pseudo", "Jeu", "Console", "Date de l'emprunt", "Date de rendu maximale", "Prolongations effectuée", "Lieu"
        };

        LinkedList<String[]> records = db.getRecords("liste_emprunts_date", "Pseudo",                
                "NomJeu_fk",
                "NomConsole_fk",
                "DateEmprunt",
                "date_fin",
                "NombreProlongation",
                "NPA_fk"
                );

        System.out.println(StringFormatHelper.getFormatedString(titles, records));
        return null;
    }
    
}
