/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class InsertShopPage extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println("Insertion magasin");

        // ------ DEMANDES
        System.out.print("Ville: ");
        String ville = sc.nextLine();
        System.out.print("Rue: ");
        String rue = sc.nextLine();
        System.out.print("NPA: ");
        int NPA = getInt();
        System.out.print("Numero rue: ");
//        String numRue = sc.nextLine();
        int numRue = getInt();

        if (db.insertShop(ville, rue, numRue, NPA)) {
            System.out.println("Insertion du magasin situe à " + ville + " reussie");
            return null;
        }
        
        System.out.println("Erreur, réessayez l'insertion.");
        return new InsertShopPage();
    }
}
