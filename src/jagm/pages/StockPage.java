/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

/**
 *
 * @author Rémi
 */
public class StockPage extends MenuPage {

    public StockPage() {
        actions.add(new Action("Lister les genres disponibles", new GenrePage()));
        actions.add(new Action("Lister les editeurs disponibles", new EditeurPage()));
        actions.add(new Action("Inserer un jeu", new InsertGamePage()));
        actions.add(new Action("Inserer une console", new InsertConsolePage()));
        actions.add(new Action("Inserer un genre", new InsertTypePage()));
        actions.add(new Action("Inserer un editeur", new InsertEditorPage()));
        actions.add(new Action("Inserer un etat", new InsertStatePage()));
        actions.add(new Action("Modifier un jeu", new UpdateGamePage()));
        actions.add(new Action("Modifier une console", new UpdateConsolePage()));
        actions.add(new Action("Modifier un genre", new UpdateTypePage()));
        actions.add(new Action("Modifier un editeur", new UpdateEditorPage()));
        actions.add(new Action("Modifier un etat", new UpdateStatePage()));
        actions.add(new Action("Supprimer un jeu", new DeleteGamePage()));
        actions.add(new Action("Supprimer une console", new DeleteConsolePage()));
        actions.add(new Action("Supprimer un genre", new DeleteTypePage()));
        actions.add(new Action("Supprimer un editeur", new DeleteEditorPage()));
        actions.add(new Action("Supprimer un etat", new DeleteStatePage()));
    }

}
