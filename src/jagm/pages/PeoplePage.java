/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Rémi
 */
public class PeoplePage extends MenuPage {

    public PeoplePage() {
        actions.add(new Action("Ajouter un employé", new AddEmployeePage()));
        actions.add(new Action("Modifier un employé", new UpdateEmployeePage()));
        actions.add(new Action("Supprimer un employé", new DeleteEmployeePage()));
    }

    @Override
    public Page show() throws SQLException {
        System.out.println("Employés actuels : ");
        String[] titles = new String[]{
            "Nom", "Prénom", "Pseudo", "Age", "Rue", "Ville", "NPA", "Mail", "N°Téléphone", "Salaire"};

        LinkedList<String[]> records = db.getRecords("voir_profile_employe", "Nom", "Prenom",
                "Pseudo", "Age", "AdrRue", "AdrVille", "AdrNPA", "Mail", "NumTel", "Salaire");

        System.out.println(StringFormatHelper.getFormatedString(titles, records));
        
        return super.show();
    }
    
    
    
    

}
