/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author aurel
 */
public class SamplePage extends Page {

    /* public boolean selectExemplaireConsole(String nom) throws SQLException {
        String query = "SELECT * FROM Exemplaire WHERE NomConsole_fk = ?";
        PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, nom);
        statement.executeUpdate();
        return true;
    }*/
    @Override
    public Page show() throws SQLException {
        System.out.println("Voulez-vous afficher des exemplaires de console (1) ou de jeu (2)?");
        int choix = getInt();

        switch (choix) {

            case 1:
                System.out.print("Nom de la console: ");
                String nom = sc.nextLine();

                String[] titles = new String[]{
                    "Id", "NumExemplaire", "EtatCommentaire", "TitreEtat_fk", "NPA_fk", "NomConsole_fk", "NomJeu_fk"
                };

                LinkedList<String[]> records = db.getRecordsWithFilter("stock_exemplaires_consoles", "NomConsole_fk", nom,
                        "Id",
                        "NumExemplaire",
                        "EtatCommentaire",
                        "TitreEtat_fk",
                        "NPA_fk",
                        "NomConsole_fk",
                        "NomJeu_fk"
                );

                System.out.println(StringFormatHelper.getFormatedString(titles, records));
                return null;
            case 2:
                System.out.print("Nom du jeu: ");
                String jeu = sc.nextLine();

                String[] titlesJeu = new String[]{
                    "Id", "NumExemplaire", "EtatCommentaire", "TitreEtat_fk", "NPA_fk", "NomConsole_fk", "NomJeu_fk"
                };

                LinkedList<String[]> recordsJeu = db.getRecordsWithFilter("stock_exemplaires_jeu", "NomJeu_fk", jeu,
                        "Id",
                        "NumExemplaire",
                        "EtatCommentaire",
                        "TitreEtat_fk",
                        "NPA_fk",
                        "NomConsole_fk",
                        "NomJeu_fk"
                );

                System.out.println(StringFormatHelper.getFormatedString(titlesJeu, recordsJeu));
                return null;
                
            default:
                return null;

        }
    }
}
