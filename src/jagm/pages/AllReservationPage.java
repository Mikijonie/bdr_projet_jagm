/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Nathan
 */
class AllReservationPage extends Page {

    public AllReservationPage() {
    }

    @Override
    public Page show() throws SQLException {
        String[] titles = new String[]{
                "N°", "Pseudo", "Jeu", "Console", "Date de réservation", "Date de retrait souhaité", "Lieu", "Id"
            };

            LinkedList<String[]> records = db.getRecords("liste_reservation",
                    "Pseudo",                
                    "NomJeu_fk",
                    "NomConsole_fk",
                    "DateReservation",
                    "DateRetraitSouhaite",
                    "NPA_fk",
                    "Id"
                    );

            LinkedList<String[]> nReservation = new LinkedList<>();
            //Ajout du numéro devant
            for (int i = 0; i < records.size(); i++) {
                String[] game = new String[records.get(i).length + 1];
                game[0] = "" + (i + 1);
                for (int j = 1; j < game.length; j++) {
                    game[j] = records.get(i)[j - 1];
                }
                nReservation.add(game);
            }

            System.out.println(StringFormatHelper.getFormatedString(titles, nReservation));
            return null;
    }   
    
}
