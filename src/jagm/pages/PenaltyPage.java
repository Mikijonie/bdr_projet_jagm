/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Rémi
 */
public class PenaltyPage extends Page {


    @Override
    public Page show() throws SQLException {
        String[] titles = new String[]{
            "Date", "Jeu"};
        
        LinkedList<String[]> records = db.getRecordsWithFilter("liste_amendes", "personne_id", db.getUsername(),
                "DateInitial", "NomJeu_fk");
        
        System.out.println(StringFormatHelper.getFormatedString(titles, records));
        return null;
    }
    
}
