/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class InsertSampleGame extends Page{

    @Override
    public Page show() throws SQLException {
        System.out.println("Insertion d'un exemplaire d'un jeu");

        // ------ DEMANDES
        System.out.print("Quel jeu voulez-vous inserer? - ");
        String titre = sc.nextLine();

        if (!db.exists("Jeu", "Titre", titre)) {
            System.err.println("Le jeu '" + titre + "' n'existe pas");
            return null;
        }else{
            System.out.print("Numéro de l'exemplaire: ");
            int numExemplaire = getInt();

            if (db.exists("Exemplaire", "NumExemplaire", numExemplaire)) {
                System.err.println("Le numéro d'exemplaire '" + numExemplaire + "' existe déjà");
                return null;
            }

            System.out.print("Commentaire sur le jeu: ");
            String etatJeu = sc.nextLine();
            
            System.out.print("Etat du jeu: ");
            String titreEtat = sc.nextLine();
            
            if (!db.exists("Etat", "Titre", titreEtat)) {
                System.err.println("L'état de la console '" + titreEtat + "' n'existe pas");
                return null;
            }

            System.out.print("NPA du magasin: ");
            int npa = getInt();

            if (!db.exists("Magasin", "NPA", npa)) {
                System.err.println("Le NPA '" + npa + "' d'un magasin n'existe pas");
                return null;
            }

            System.out.print("Nom de la console: ");
            String nomConsoleFk = sc.nextLine();
            
            if (!db.exists("Console", "Nom", nomConsoleFk)) {
                System.err.println("La console '" + nomConsoleFk + "' n'existe pas");
                return null;
            }

            db.insertSampleGame(numExemplaire, etatJeu, titreEtat, npa, nomConsoleFk, titre);
            
            System.out.println("Insertion de '" + titre + "' reussie");
            return null;
        }
    }
    
}
