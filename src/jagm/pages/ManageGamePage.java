/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

/**
 *
 * @author Rémi
 */
public class ManageGamePage extends MenuPage {

    public ManageGamePage() {
        actions.add(new MenuPage.Action("Inserer un exemplaire de jeu", new InsertSampleGame()));
        actions.add(new MenuPage.Action("Modifier un exemplaire de jeu", new UpdateSampleGame()));
        actions.add(new MenuPage.Action("Supprimer un exemplaire de jeu", new DeleteSampleGame()));
    }

}
