/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class UpdateTypePage extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println("Modification Genre");

        // ------ DEMANDES
        System.out.print("Titre du genre ");
        String titreOld = sc.nextLine();
        if (!db.exists("Genre", "Titre", titreOld)) {
            System.err.println("Le genre " + titreOld + " n'existe pas");
            return null;
        }

        System.out.print("Nouveau titre : ");
        String TitreNew = sc.nextLine();

        db.updateGenre(TitreNew, titreOld);
        System.out.println("Modification de " + TitreNew + " reussie. Anciennement " + titreOld);
        return null;
        
    }
}
