/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.JagmBDD;
import jagm.StringFormatHelper;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;

/**
 *
 * @author Börk
 */
public class DeleteReservationPage extends Page {

    public DeleteReservationPage() {
    }

    @Override
    public Page show() throws SQLException {
        
        LinkedList<String[]> records;
        int index = 5;
        
        if(db.getUserRight().equals(JagmBDD.UserRights.Admin)){
            index = 6;
            String[] titles = new String[]{
                "N°", "Pseudo", "Jeu", "Console", "Date de réservation", "Date de retrait souhaité", "Lieu", "Id"
            };

            records = db.getRecords("liste_reservation",
                    "Pseudo",                
                    "NomJeu_fk",
                    "NomConsole_fk",
                    "DateReservation",
                    "DateRetraitSouhaite",
                    "NPA_fk",
                    "Id"
                    );

            LinkedList<String[]> nReservation = new LinkedList<>();
            //Ajout du numéro devant
            for (int i = 0; i < records.size(); i++) {
                String[] game = new String[records.get(i).length + 1];
                game[0] = "" + (i + 1);
                for (int j = 1; j < game.length; j++) {
                    game[j] = records.get(i)[j - 1];
                }
                nReservation.add(game);
            }

            System.out.println(StringFormatHelper.getFormatedString(titles, nReservation));
            
        
        } else {
            String[] titles = new String[]{
                "N°", "Pseudo", "Jeu", "Console", "Date de réservation", "Date de retrait souhaité", "Lieu"
            };

            records = db.getRecordsWithFilter("liste_reservation", "Pseudo", db.getUsername(),
                    "NomJeu_fk",
                    "NomConsole_fk",
                    "DateReservation",
                    "DateRetraitSouhaite",
                    "NPA_fk",
                    "Id"
                    );
            
            LinkedList<String[]> nReservation = new LinkedList<>();
            //Ajout du numéro devant
            for (int i = 0; i < records.size(); i++) {
                String[] game = new String[records.get(i).length + 1];
                game[0] = "" + (i + 1);
                for (int j = 1; j < game.length; j++) {
                    game[j] = records.get(i)[j - 1];
                }
                nReservation.add(game);
            }
                
            LinkedList<String[]> filtredEmprunts = new LinkedList<>();
            for (String[] row : nReservation) {
                filtredEmprunts.add(Arrays.copyOfRange(row, 0, 7));
            }
            
            //Affichage de la vue filtrée
            System.out.println(StringFormatHelper.getFormatedString(titles, filtredEmprunts));
        }
        
            System.out.print("Quel réservation souhaitez vous supprimer ? ");
            int choice = getInt();
            
            if(choice >= 1 && choice <= records.size()) {
                String s = records.get(choice-1)[index];
                
                int idChoice = Integer.parseInt(s);
                
                if(db.deleteReservation(idChoice) == 0){
                    System.out.println("La suppression s'est déroulée avec succès ! ");
                } else {
                    System.err.println("Erreur lors de la supression de la réservation...");
                }
            }
        return null;
    }
    
}
