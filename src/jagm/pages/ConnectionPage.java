/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author Rémi
 */
public class ConnectionPage extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println("Bienvenue");

        System.out.print("Entrez votre nom d'utilisateur : ");
        String user = sc.nextLine();

        System.out.print("Entrez votre mot de passe : ");
        String password = sc.nextLine();

        if (db.connectUser(user, password)) {
            //L'utilisateur est bien connecté
            return new OptionsPage();
        } else {
            System.out.println("Mauvaise combinaison user/mdp");
        }
        
        return null;
    }

}
