/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class InsertEditorPage extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println("Insertion editeur");

        // ------ DEMANDES
        System.out.print("Nom: ");
        String nom = sc.nextLine();
        if (db.exists("Editeur", "Nom", nom)) {
            System.err.println("L'editeur " + nom + " existe déjà");
            return null;
        }

        db.insertEditeur(nom);
        System.out.println("Insertion de " + nom + " reussie");
        return null;

    }
}
