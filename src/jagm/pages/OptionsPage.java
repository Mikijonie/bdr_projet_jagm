/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import jagm.JagmBDD.*;
import jagm.JagmBDD.UserRights;
import java.sql.SQLException;

/**
 *
 * @author Rémi
 */
public class OptionsPage extends MenuPage {

    public OptionsPage() {
        actions.clear();
        actions.add(new Action("Déconnexion"));

        if (db.getUserRight().equals(UserRights.Admin)) {
            actions.add(new Action("Gérer les magasins existants", new MagasinPage()));
            actions.add(new Action("Lister les etats disponibles", new EtatPage()));
            actions.add(new Action("Gérer le stock", new StockPage()));
            actions.add(new Action("Gérer les exemplaires", new ManagementSamplePage()));
            actions.add(new Action("Gérer les emprunts", new HandleLoanPage()));
            actions.add(new Action("Gérer les réservations", new HandleReservationPage()));
            actions.add(new Action("Gérer les amendes", new HandlePenaltyPage()));
            actions.add(new Action("Gérer les personnes", new PeoplePage()));
            actions.add(new Action("Voir mon profile", new EmployeeProfilPage()));
        } else {
            actions.add(new Action("Lister les exemplaires d'un jeu ou d'une console disponibles", new SamplePage()));
            actions.add(new Action("Lister les exemplaires de jeux disponibles", new GamePage()));
            actions.add(new Action("Lister les exemplaires de consoles disponibles", new ConsolePage()));
            actions.add(new Action("Lister mes emprunts actuels", new MemberLoanPage()));
            actions.add(new Action("Gérer mes réservations", new MemberReservationPage()));
            actions.add(new Action("Voir mes amendes", new PenaltyPage()));
            actions.add(new Action("Lister les avis", new ReviewPage()));
            actions.add(new Action("Ajouter un avis", new AddReviewPage()));
            actions.add(new Action("Voir mon profile", new MemberProfilPage()));
        }
    }

    @Override
    public Page show() throws SQLException {
        System.out.println("Bienvenue " + db.getUsername()
                + "! (" + db.getUserRight() + ")");
        System.out.println("-1 - Quitter");

        return super.show();
    }

}
