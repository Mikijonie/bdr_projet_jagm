/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class DeleteTypePage extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println("Suppression genre");

        // ------ DEMANDES
        System.out.print("Titre : ");
        String titre = sc.nextLine();

        try {
            if (!db.exists("Genre", "Titre", titre)) {
                System.err.println("Le genre " + titre + " n'existe pas");

            } else {
                db.deleteGenre(titre);
                System.out.println("Suppression de " + titre + " reussie");
            }
        } catch (MySQLIntegrityConstraintViolationException ex) {
            System.err.println("Des jeux utilises encore ce genre !");
        }

        return null;
    }

}
