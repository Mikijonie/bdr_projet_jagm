/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm.pages;

import java.sql.SQLException;

/**
 *
 * @author aurel
 */
public class UpdateSampleConsole extends Page {

    @Override
    public Page show() throws SQLException {
        System.out.println("Modification exemplaire console");

        // ------ DEMANDES
        System.out.print("Nom de la console a modifier: ");
        String nomConsoleOld = sc.nextLine();
        System.out.print("Numero de l'exemplaire a modifier: ");
        int numExOld = getInt();
        System.out.print("Nouveau nom de la console: ");
        String nomConsoleNew = sc.nextLine();
        System.out.print("Nouveau numero de l'exemplaire: ");
        int numExNew = getInt();
        System.out.print("Nouveau titre etat de la console: ");
        String titreEtat = sc.nextLine();
        System.out.print("Nouveau commentaire etat de la console: ");
        String EtatComment = sc.nextLine();

        System.out.print("Nouveau NPA du magasin où est la console: ");
        int NPA = getInt();
        String whereCondition =  "NomConsole_fk = '" + nomConsoleOld + "' and NumExemplaire = '" + numExOld + "'";
        if (!db.exists("exemplaire",whereCondition)) {
            System.err.println("L'exemplaire num " + numExOld + " de la console " + nomConsoleOld + " n'existe pas");
            return null;
        }
        
        db.updateSampleConsole(numExOld, numExNew, EtatComment, titreEtat, nomConsoleOld, nomConsoleNew, NPA);
        System.out.println("Modification de l'exemplaire num " + numExOld + " de la console " + nomConsoleOld + " reussie.");
        return null;

    }
}
