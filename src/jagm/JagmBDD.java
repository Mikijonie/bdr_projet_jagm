/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jagm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

/**
 *
 * @author Rémi
 */
public class JagmBDD {
    
    private static JagmBDD instance;
    private final Connection connection;
    
    public Connection getConnection() {
        return connection;
    }
    
    private JagmBDD() throws SQLException {
        this.connection = DriverManager.getConnection("jdbc:mysql://localhost/jagm", "root", "root");
    }

    //Retourne le singleton de la base de donnée
    public static JagmBDD getInstance() throws SQLException {
        if (instance == null) {
            instance = new JagmBDD();
        }
        
        return instance;
    }

    /**
     * Crée un nouvel utilisateur
     *
     * @param nom
     * @param prenom
     * @param user
     * @param password
     * @param age
     * @param ville
     * @param NPA
     * @param numTel
     * @param rue
     * @param mail
     * @return vrai si l'utilisateur a pu être créé
     * @throws java.sql.SQLException
     */
    public boolean createUser(String nom, String prenom, String user, String password,
            int age, int NPA, String rue, String ville, String mail, String numTel) throws SQLException {
        
        if (exists("personne", "Pseudo", user)) { //L'utilisateur existe                
            return false;
        }
        
        PreparedStatement personneState
                = connection.prepareStatement("INSERT INTO Personne (\n"
                        + "    Nom,\n"
                        + "    Prenom,\n"
                        + "    Pseudo,\n"
                        + "    MotDePasse,\n"
                        + "    Age,\n"
                        + "    AdrNPA,\n"
                        + "    AdrRue,\n"
                        + "    AdrVille,\n"
                        + "    Mail,\n"
                        + "    NumTel,\n"
                        + "    DateSuppression,\n"
                        + "    DateInscription,\n"
                        + "    DroitTitre_fk)\n"
                        + "    VALUES\n"
                        + "    (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NULL, current_date(), 'User');");
        
        personneState.setString(1, nom);
        personneState.setString(2, prenom);
        personneState.setString(3, user);
        personneState.setString(4, password);
        personneState.setInt(5, age);
        personneState.setInt(6, NPA);
        personneState.setString(7, rue);
        personneState.setString(8, ville);
        personneState.setString(9, mail);
        personneState.setString(10, numTel);

        //ResultSet set = personneState.executeQuery();
        PreparedStatement userState
                = connection.prepareStatement("INSERT INTO membre ("
                        + "    Id_fk) "
                        + "    VALUES "
                        + "    ((SELECT Id FROM Personne WHERE Pseudo = ? ) );");
        userState.setString(1, user);
        
        personneState.executeUpdate();
        userState.executeUpdate();
        
        return true;
    }

    /**
     * Crée un nouvel employé
     * 
     * @param nom
     * @param prenom
     * @param user
     * @param password
     * @param age
     * @param ville
     * @param NPA
     * @param numTel
     * @param rue
     * @param mail
     * @return vrai si l'utilisateur a pu être créé
     * @throws java.sql.SQLException
     * */
    public boolean createEmployee(String nom, String prenom, String user, String password,
            int age, int NPA, String rue, String ville, String mail, String numTel, double salary, int NPAMagasin) throws SQLException {
        
        if (exists("personne", "Pseudo", user) || !exists("Magasin", "NPA", NPAMagasin)) { //L'utilisateur existe                
            return false;
        }
        
        PreparedStatement personneState
                = connection.prepareStatement("INSERT INTO Personne (\n"
                        + "    Nom,\n"
                        + "    Prenom,\n"
                        + "    Pseudo,\n"
                        + "    MotDePasse,\n"
                        + "    Age,\n"
                        + "    AdrNPA,\n"
                        + "    AdrRue,\n"
                        + "    AdrVille,\n"
                        + "    Mail,\n"
                        + "    NumTel,\n"
                        + "    DateSuppression,\n"
                        + "    DateInscription,\n"
                        + "    DroitTitre_fk)\n"
                        + "    VALUES\n"
                        + "    (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NULL, current_date(), 'Admin');");
        
        personneState.setString(1, nom);
        personneState.setString(2, prenom);
        personneState.setString(3, user);
        personneState.setString(4, password);
        personneState.setInt(5, age);
        personneState.setInt(6, NPA);
        personneState.setString(7, rue);
        personneState.setString(8, ville);
        personneState.setString(9, mail);
        personneState.setString(10, numTel);

        //ResultSet set = personneState.executeQuery();
        PreparedStatement userState
                = connection.prepareStatement("INSERT INTO Employe ("
                        + "    Id_fk, NPA_fk, Salaire) "
                        + "    VALUES "
                        + "    ((SELECT Id FROM Personne WHERE Pseudo = ? ), ?, ? );");
        userState.setString(1, user);
        userState.setInt(2, NPAMagasin);
        userState.setDouble(3, salary);
        
        personneState.executeUpdate();
        userState.executeUpdate();
        
        return true;
    }
    
    public boolean updateEmployee(String oldPseudo, String nom, String prenom, String user, String password,
            int age, int NPA, String rue, String ville, String mail, String numTel, double salary, int NPAMagasin) throws SQLException {

        //Recupère l'id
        PreparedStatement state = connection.prepareStatement("SELECT Id FROM personne WHERE Pseudo = ?");
        state.setString(1, oldPseudo);
        ResultSet set = state.executeQuery();
        if (!set.next()) //Pas de personne associée au pseudo
        {
            return false;
        }
        
        int id = set.getInt("Id");
        if (!exists("Employe", "Id_fk", id)) //Pas d'employé associé au pseudo
        {
            return false;
        }
        
        if (!exists("Magasin", "NPA", NPAMagasin)) //Le magasin n'existe pas
        {
            return false;
        }
        
        PreparedStatement personneState
                = connection.prepareStatement("UPDATE Personne SET"
                        + "    Nom = ?,"
                        + "    Prenom = ?,"
                        + "    Pseudo = ?,"
                        + "    MotDePasse = ?,"
                        + "    Age = ?,"
                        + "    AdrNPA = ?,"
                        + "    AdrRue = ?,"
                        + "    AdrVille = ?,"
                        + "    Mail = ?,"
                        + "    NumTel = ?"
                        + "    WHERE Pseudo = ?;");
        
        personneState.setString(1, nom);
        personneState.setString(2, prenom);
        personneState.setString(3, user);
        personneState.setString(4, password);
        personneState.setInt(5, age);
        personneState.setInt(6, NPA);
        personneState.setString(7, rue);
        personneState.setString(8, ville);
        personneState.setString(9, mail);
        personneState.setString(10, numTel);
        personneState.setString(11, oldPseudo);

        //ResultSet set = personneState.executeQuery();
        PreparedStatement userState
                = connection.prepareStatement("UPDATE Employe SET"
                        + "    NPA_fk  = ?, Salaire = ? "
                        + "    WHERE Id_fk = ?;");
        userState.setInt(1, NPAMagasin);
        userState.setDouble(2, salary);
        userState.setInt(3, id);
        
        personneState.executeUpdate();
        userState.executeUpdate();
        
        return true;
    }
    
    public boolean insertReview(String titleChoice, int note, String comment) throws SQLException {
        if (!exists("avis", "JeuTitre_fk", titleChoice)) //Verifie l'existance du Jeu
        {
            return false;
        }
        
        String insertReviewSql = "INSERT INTO avis (Note, Commentaire, JeuTitre_fk) VALUES(?,?,?)";
        PreparedStatement statement = connection.prepareStatement(insertReviewSql);
        
        statement.setInt(1, note);
        statement.setString(3, titleChoice);
        if (comment == null) {
            statement.setNull(2, java.sql.Types.VARCHAR);
        } else {
            statement.setString(2, comment);
        }
        
        statement.executeUpdate();
        
        return true;
    }
    
    public void insertLoan(int member, String titleChoice, String consoleChoice, int numDay) throws SQLException {
        //Récupère l'ID de l'exemplaire
        String getExemplaireSql = "SELECT Id , NumExemplaire "
                + "FROM jeux_disponibles_avec_ex "
                + "WHERE (NomJeu_fk = ? AND NomConsole_fk = ? ) "
                + "LIMIT 1";
        PreparedStatement exemplaireStatement = connection.prepareStatement(getExemplaireSql);
        exemplaireStatement.setString(1, titleChoice);
        exemplaireStatement.setString(2, consoleChoice);
        ResultSet exSet = exemplaireStatement.executeQuery();
        
        if (!exSet.next()) { //Il n'y a pas d'enregistrement, on quitte
            return;
        }
        
        String insertLoanSql = "INSERT INTO emprunt("
                + "DateEmprunt,"
                + "DureeJour,"
                + "NombreProlongation,"
                + "IdMembre_fk,"
                + "IdExemplaire_fk,"
                + "NumExemplaire_fk) VALUES"
                + "(current_date(), ?, 0, ?, ?, ?);";
        PreparedStatement statement = connection.prepareStatement(insertLoanSql);
        statement.setInt(1, numDay);
        statement.setInt(2, member);
        statement.setInt(3, exSet.getInt("Id"));
        statement.setInt(4, exSet.getInt("NumExemplaire"));
        
        statement.executeUpdate();
    }
    
    public void insertReservation(String titleChoice, String consoleChoice, int userId) throws SQLException {
        String getExemplaireSql;
        if (titleChoice == null || titleChoice.equals("")) {
            getExemplaireSql = "SELECT Id, NumExemplaire "
                    + "FROM Exemplaire "
                    + "WHERE (NomJeu_fk IS NULL AND NomConsole_fk = ? ) "
                    + "LIMIT 1";
        } else //Récupère l'ID de l'exemplaire
        {
            getExemplaireSql = "SELECT Id, NumExemplaire "
                    + "FROM Exemplaire "
                    + "WHERE (NomJeu_fk = ? AND NomConsole_fk = ? ) "
                    + "LIMIT 1";
            
        }
        PreparedStatement exemplaireStatement = connection.prepareStatement(getExemplaireSql);
        
        if (titleChoice == null || titleChoice.equals("")) {
            exemplaireStatement.setString(1, consoleChoice);
        } else {
            exemplaireStatement.setString(1, titleChoice);
            exemplaireStatement.setString(2, consoleChoice);
        }
        
        ResultSet exSet = exemplaireStatement.executeQuery();
        
        if (!exSet.next()) { //Il n'y a pas d'enregistrement, on quitte
            return;
        }
        
        String insertReservationSql = "INSERT INTO reservation("
                + "DateReservation,"
                + "DateRetraitSouhaite,"
                + "IdMembre_fk,"
                + "IdExemplaire_fk,"
                + "NumExemplaire_fk) VALUES"
                + "(current_date(), DATE_ADD(DateReservation,INTERVAL 10 DAY), ?, ?, ?);";
        PreparedStatement statement = connection.prepareStatement(insertReservationSql);
        
        statement.setInt(1, userId);
        statement.setInt(2, exSet.getInt("Id"));
        statement.setInt(3, exSet.getInt("NumExemplaire"));
        
        statement.executeUpdate();
    }

    //-------------- CONNECTION D'UN UTILISATEUR
    public enum UserRights {
        Admin, User
    };
    private String connectedUser = null;
    
    public String getUsername() {
        return connectedUser;
    }
    private UserRights userRight = null;
    
    public UserRights getUserRight() {
        return userRight;
    }
    
    private int userId;
    
    public int getUserId() {
        return userId;
    }

    /**
     *
     * @param user
     * @param mdp
     * @return Si l'utilisateur s'est bien connecté
     */
    public boolean connectUser(String user, String mdp) throws SQLException {
        PreparedStatement state = connection.prepareStatement("SELECT DroitTitre_fk AS droit, Id FROM personne WHERE Pseudo = ? AND MotDePasse = ?");
        state.setString(1, user);
        state.setString(2, mdp);
        ResultSet set = state.executeQuery();
        
        if (set.next()) { //L'utilisateur existe                
            this.connectedUser = user;
            this.userRight = UserRights.valueOf(set.getString("droit"));
            this.userId = set.getInt("Id");
            return true;
        }
        this.connectedUser = null;
        this.userRight = null;
        return false;
        
    }
    
    public void insertGame(String titre, int anneeProduction, int limiteAge, String nomConsoleFk, String nomEditeurFk, String GenreTitreFk) throws SQLException {
        String insertGameSql = "INSERT INTO Jeu (Titre, AnneeProduction, LimiteAge, NomConsole_fk, NomEditeur_fk, GenreTitre_fk) VALUES(?,?,?,?,?,?)";
        PreparedStatement statement = connection.prepareStatement(insertGameSql);
        
        statement.setString(1, titre);
        statement.setInt(2, anneeProduction);
        if (limiteAge != 0) {//limiteAge vaut 0 de base
            statement.setInt(3, limiteAge);
        }
        statement.setString(4, nomConsoleFk);
        statement.setString(5, nomEditeurFk);
        statement.setString(6, GenreTitreFk);
        
        statement.executeUpdate();
    }
    
    public void updateGame(String titre, int anneeProduction, int limiteAge, String nomConsoleFk, String nomEditeurFk, String GenreTitreFk, String TitreAModif) throws SQLException {
        String insertGameSql = "UPDATE Jeu SET Titre = ?, AnneeProduction = ?, LimiteAge = ?, NomConsole_fk = ?, NomEditeur_fk = ?, GenreTitre_fk = ? where Titre = ?";
        PreparedStatement statement = connection.prepareStatement(insertGameSql);
        
        statement.setString(7, TitreAModif);
        
        statement.setString(1, titre);
        
        statement.setInt(2, anneeProduction);
        
        statement.setInt(3, limiteAge);
        
        statement.setString(4, nomConsoleFk);
        
        statement.setString(5, nomEditeurFk);
        
        statement.setString(6, GenreTitreFk);
        
        statement.executeUpdate();
    }
    
    public void deleteGame(String titre) throws SQLException {
        String deleteGameSql = "DELETE FROM Jeu WHERE Titre = ?";
        PreparedStatement statement = connection.prepareStatement(deleteGameSql);
        statement.setString(1, titre);
        statement.executeUpdate();
    }

    //-------------- REQUETES --------------------
    public class Game {
        
        public int nbr_ex_jeux;
        public String titreJeu;
        public String titreConsole;
    }
    
    public LinkedList<Game> getFreeGames() throws SQLException {
        LinkedList<Game> games = new LinkedList<>();
        Statement state = connection.createStatement();
        ResultSet set = state.executeQuery("SELECT nbr_ex_jeux, NomJeu_fk, NomConsole_fk FROM jeux_disponibles");
        
        while (set.next()) {
            Game g = new Game();
            g.nbr_ex_jeux = set.getInt("nbr_ex_jeux");
            g.titreJeu = set.getString("NomJeu_fk");
            g.titreConsole = set.getString("NomConsole_fk");
            games.add(g);
        }
        
        return games;
    }
    
    public void insertConsole(String titre, int anneeProduction) throws SQLException {
        String insertGameSql = "INSERT INTO Console (Nom, AnneeProduction) VALUES(?,?)";
        PreparedStatement statement = connection.prepareStatement(insertGameSql);
        
        statement.setString(1, titre);
        statement.setInt(2, anneeProduction);
        
        statement.executeUpdate();
    }
    
    public void updateConsole(String titre, int anneeProduction, String titreModif) throws SQLException {
        String insertGameSql = "UPDATE Console SET Nom = ?, AnneeProduction = ? WHERE Nom = ?";
        PreparedStatement statement = connection.prepareStatement(insertGameSql);
        
        statement.setString(3, titreModif);
        
        statement.setString(1, titre);
        
        statement.setInt(2, anneeProduction);
        
        statement.executeUpdate();
    }
    
    public void deleteConsole(String Nom) throws SQLException {
        String deleteGameSql = "DELETE FROM Console WHERE Nom = ?";
        PreparedStatement statement = connection.prepareStatement(deleteGameSql);
        statement.setString(1, Nom);
        statement.executeUpdate();
    }
    
    public class Console {
        
        public int nbr_ex_consoles;
        public String titreConsole;
        
    }
    
    public LinkedList<Console> getFreeConsole() throws SQLException {
        LinkedList<Console> consoles = new LinkedList<>();
        Statement state = connection.createStatement();
        ResultSet set = state.executeQuery("SELECT NomConsole_fk, nbr_ex_consoles FROM consoles_disponibles");
        
        while (set.next()) {
            Console c = new Console();
            c.nbr_ex_consoles = set.getInt("nbr_ex_consoles");
            c.titreConsole = set.getString("NomConsole_fk");
            consoles.add(c);
        }
        
        return consoles;
    }
    
    public LinkedList<String[]> getRecords(String table, String... cols) throws SQLException {
        String query = "SELECT ";

        //On prends les colonnes à sélectionner
        for (int i = 0; i < cols.length; i++) {
            query += cols[i];
            if (i < cols.length - 1) {
                query += ", ";
            }
        }
        
        query += " FROM " + table;
        
        Statement state = connection.createStatement();
        ResultSet set = state.executeQuery(query);
        
        LinkedList<String[]> records = new LinkedList<>();
        int nbrCols = set.getMetaData().getColumnCount();
        while (set.next()) {
            String[] record = new String[nbrCols];
            
            for (int i = 0; i < nbrCols; i++) {
                record[i] = set.getString(i + 1);
                if (record[i] == null) {
                    record[i] = "";
                }
            }
            
            records.add(record);
        }
        
        return records;
        
    }
    
    public LinkedList<String[]> getRecordsWithFilter(String table, String colToFilter, String valueForFilter, String... cols) throws SQLException {
        String query = "SELECT ";

        //On prends les colonnes à sélectionner
        for (int i = 0; i < cols.length; i++) {
            query += cols[i];
            if (i < cols.length - 1) {
                query += ", ";
            }
        }
        
        query += " FROM " + table;
        query += " WHERE " + colToFilter + " = '" + valueForFilter + "'";
        
        Statement state = connection.createStatement();
        ResultSet set = state.executeQuery(query);
        
        LinkedList<String[]> records = new LinkedList<>();
        int nbrCols = set.getMetaData().getColumnCount();
        while (set.next()) {
            String[] record = new String[nbrCols];
            
            for (int i = 0; i < nbrCols; i++) {
                record[i] = set.getString(i + 1);
                if (record[i] == null) {
                    record[i] = "";
                }
            }
            
            records.add(record);
        }
        return records;
        
    }
    
    public String[] getRecord(String table, String colToFilter, String valueForFilter, String... cols) throws SQLException {
        String query = "SELECT ";

        //On prends les colonnes à sélectionner
        for (int i = 0; i < cols.length; i++) {
            query += cols[i];
            if (i < cols.length - 1) {
                query += ", ";
            }
        }
        
        query += " FROM " + table;
        query += " WHERE " + colToFilter + " = '" + valueForFilter + "'";
        
        Statement state = connection.createStatement();
        ResultSet set = state.executeQuery(query);
        
        String[] record = null;
        int nbrCols = set.getMetaData().getColumnCount();
        if (set.next()) {
            record = new String[nbrCols];
            
            for (int i = 0; i < nbrCols; i++) {
                record[i] = set.getString(i + 1);
                if (record[i] == null) {
                    record[i] = "";
                }
            }
            
        }
        
        return record;
    }
    
    public void insertGenre(String Titre) throws SQLException {
        String insertGenreSql = "INSERT INTO Genre (Titre) VALUES(?)";
        PreparedStatement statement = connection.prepareStatement(insertGenreSql);
        
        statement.setString(1, Titre);
        
        statement.executeUpdate();
    }
    
    public void deleteGenre(String titre) throws SQLException {
        String deleteGenreSql = "DELETE FROM Genre WHERE Titre = ?";
        PreparedStatement statement = connection.prepareStatement(deleteGenreSql);
        statement.setString(1, titre);
        statement.executeUpdate();
    }
    
    public void updateGenre(String titreNew, String titreOld) throws SQLException {
        String updateGenre = "UPDATE Genre SET Titre = ? WHERE Titre = ?";
        PreparedStatement statement = connection.prepareStatement(updateGenre);
        
        statement.setString(1, titreNew);
        statement.setString(2, titreOld);
        statement.executeUpdate();
    }
    
    public void insertEditeur(String nom) throws SQLException {
        String insertEditeurSql = "INSERT INTO Editeur (Nom) VALUES(?)";
        PreparedStatement statement = connection.prepareStatement(insertEditeurSql);
        
        statement.setString(1, nom);
        
        statement.executeUpdate();
    }
    
    public void updateEditeur(String nomNew, String nomOld) throws SQLException {
        String updateEditeur = "UPDATE Editeur SET Nom = ? WHERE Nom = ?";
        PreparedStatement statement = connection.prepareStatement(updateEditeur);
        
        statement.setString(1, nomNew);
        
        statement.setString(2, nomOld);
        
        statement.executeUpdate();
    }
    
    public void deleteEditeur(String nom) throws SQLException {
        String deleteEditeur = "DELETE FROM Editeur WHERE Nom = ?";
        PreparedStatement statement = connection.prepareStatement(deleteEditeur);
        statement.setString(1, nom);
        statement.executeUpdate();
    }
    
    public void insertState(String Titre) throws SQLException {
        String insertEtatSql = "INSERT INTO Etat (Titre) VALUES(?)";
        PreparedStatement statement = connection.prepareStatement(insertEtatSql);
        
        statement.setString(1, Titre);
        
        statement.executeUpdate();
    }
    
    public void updateState(String titreNew, String titreOld) throws SQLException {
        String updateStateSql = "UPDATE Etat SET Titre = ? WHERE Titre = ?";
        PreparedStatement statement = connection.prepareStatement(updateStateSql);
        
        statement.setString(1, titreNew);
        
        statement.setString(2, titreOld);
        
        statement.executeUpdate();
    }
    
    public void deleteEtat(String Titre) throws SQLException {
        String deleteEtatSql = "DELETE FROM Etat WHERE Titre = ?";
        PreparedStatement statement = connection.prepareStatement(deleteEtatSql);
        statement.setString(1, Titre);
        statement.executeUpdate();
    }
    
    public boolean exists(String table, String colToFilter, int valueToFilter) throws SQLException {
        String existsSql = "SELECT COUNT(*) FROM " + table + " WHERE " + colToFilter + " = ?";
        PreparedStatement state = connection.prepareStatement(existsSql);
        state.setInt(1, valueToFilter);
        ResultSet set = state.executeQuery();
        
        if (set.next()) {
            return set.getInt(1) >= 1;
        }
        
        return false;
    }
    
    public boolean exists(String table, String colToFilter, String valueToFilter) throws SQLException {
        String existsSql = "SELECT COUNT(*) FROM " + table + " WHERE " + colToFilter + " = ?";
        PreparedStatement state = connection.prepareStatement(existsSql);
        state.setString(1, valueToFilter);
        ResultSet set = state.executeQuery();
        
        if (set.next()) {
            return set.getInt(1) >= 1;
        }
        
        return false;
    }
    
    public boolean exists(String table, String whereCondition) throws SQLException {
        String existsSql = "SELECT COUNT(*) FROM " + table + " WHERE " + whereCondition;
        PreparedStatement state = connection.prepareStatement(existsSql);
        //state.setString(1, valueToFilter);
        ResultSet set = state.executeQuery();
        
        if (set.next()) {
            return set.getInt(1) >= 1;
        }
        
        return false;
    }
    
    public boolean insertShop(String ville, String rue, int numRue, int NPA) throws SQLException {
        String insertShopSql = "INSERT INTO magasin (Ville, Rue, NumRue, NPA) VALUES(?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(insertShopSql);
        
        statement.setString(1, ville);
        statement.setString(2, rue);
        statement.setInt(3, numRue);
        statement.setInt(4, NPA);
        
        statement.executeUpdate();
        return true;
    }
    
    public boolean updateShop(String ville, String rue, int numRue, int NPA, String oldville, String oldRue) throws SQLException {
        String updateStateSql = "UPDATE magasin SET Ville = ?, Rue = ?, NumRue = ?, NPA = ? WHERE Ville = ? AND Rue = ?";
        PreparedStatement statement = connection.prepareStatement(updateStateSql);
        
        statement.setString(1, ville);
        statement.setString(2, rue);
        statement.setInt(3, numRue);
        
        statement.setInt(4, NPA);
        statement.setString(5, oldville);
        statement.setString(6, oldRue);
        
        statement.executeUpdate();
        return true;
    }
    
    public boolean deleteShop(String ville, String rue) throws SQLException {
        String deleteShopSql = "DELETE FROM magasin WHERE Ville = ? AND Rue = ?";
        PreparedStatement statement = connection.prepareStatement(deleteShopSql);
        
        statement.setString(1, ville);
        statement.setString(2, rue);
        
        statement.executeUpdate();
        return true;
    }

    /**
     * Deletes a Reservation in the database
     *
     * @param id The id of the reservation you want to delete
     * @return 0 if everything is okay and 1 otherwise
     * @throws SQLException
     */
    public int deleteReservation(int id) throws SQLException {
        String deleteEtatSql = "DELETE FROM reservation"
                + " WHERE id = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(deleteEtatSql);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            return 1;
        }
        return 0;
    }

    /**
     * Deletes a Loan in the database
     *
     * @param id The id of the load you want to delete
     * @return 0 if everything is okay and 1 otherwise
     * @throws SQLException
     */
    public int deleteLoan(int id) throws SQLException {
        String deleteEtatSql = "DELETE FROM emprunt"
                + " WHERE id = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(deleteEtatSql);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            return 1;
        }
        return 0;
    }
    
    public void createAmende(int emprunt, String type) throws SQLException {
        String insertAmende = "INSERT INTO Amende (DateInitial, TypeAmende_fk, EmpruntId_fk) "
                + "VALUES(now(), ?, ?)";
        PreparedStatement statement = connection.prepareStatement(insertAmende);
        
        statement.setString(1, type);
        statement.setInt(2, emprunt);
        
        statement.executeUpdate();
    }
    
    public void deleteAmende(int id) throws SQLException {
        String deleteEtatSql = "DELETE FROM Amende WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(deleteEtatSql);
        
        statement.setInt(1, id);
        statement.executeUpdate();
    }
    
    public boolean insertSampleConsole(int numEx, String etatCommentaire, String titreEtat, String nomConsole_fk, int NPA) throws SQLException {
        String insertShopSql = "INSERT INTO exemplaire (numExemplaire, EtatCommentaire, TitreEtat_fk, NomConsole_fk, NPA_fk) VALUES(?, ?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(insertShopSql);
        
        statement.setInt(1, numEx);
        statement.setString(2, etatCommentaire);
        statement.setString(3, titreEtat);
        statement.setString(4, nomConsole_fk);
        statement.setInt(5, NPA);
        
        statement.executeUpdate();
        return true;
    }
    
    public boolean updateSampleConsole(int OldnumEx, int newNumEx, String newEtatComm,
            String NewTitreEtat, String OldnomConsole_fk, String NewNomConsole_fk, int newNPA) throws SQLException {
        String updateStateSql
                = "UPDATE exemplaire SET NumExemplaire = ?, EtatCommentaire = ?, TitreEtat_fk = ?, NPA_fk = ?, NomConsole_fk= ? WHERE NomConsole_fk = ? AND NumExemplaire = ?";
        
        PreparedStatement statement = connection.prepareStatement(updateStateSql);
        
        statement.setInt(1, newNumEx);
        statement.setString(2, newEtatComm);
        statement.setString(3, NewTitreEtat);
        statement.setInt(4, newNPA);
        statement.setString(5, NewNomConsole_fk);
        statement.setString(6, OldnomConsole_fk);
        statement.setInt(7, OldnumEx);
        
        statement.executeUpdate();
        return true;
    }

    public boolean deleteSampleConsole(String nom, int num) throws SQLException {
        String deleteShopSql = "DELETE FROM exemplaire WHERE NomConsole_fk = ? AND NumExemplaire = ?";
        PreparedStatement statement = connection.prepareStatement(deleteShopSql);
        
        statement.setString(1, nom);
        statement.setInt(2, num);
        
        statement.executeUpdate();
        return true;
    }
    
    public void insertSampleGame(int numExemplaire, String etatCommentaire, String titreEtat, int npa, String nomConsoleFk, String nomJeuFk) throws SQLException {
        String insertGameSql = "INSERT INTO Exemplaire (NumExemplaire,\n"
                + "					EtatCommentaire,\n"
                + "					TitreEtat_fk,\n"
                + "					NPA_fk,\n"
                + "					NomConsole_fk,\n"
                + "					NomJeu_fk) VALUES(?,?,?,?,?,?)";
        PreparedStatement statement = connection.prepareStatement(insertGameSql);
        
        statement.setInt(1, numExemplaire);
        statement.setString(2, etatCommentaire);
        statement.setString(3, titreEtat);
        statement.setInt(4, npa);
        statement.setString(5, nomConsoleFk);
        statement.setString(6, nomJeuFk);
        
        statement.executeUpdate();
    }

    public boolean updateSampleGame(String etatCommentaire, String titreEtat, int numExemplaire, String nomJeuFk) throws SQLException {
        String insertGameSql = "UPDATE Exemplaire SET EtatCommentaire = ?, TitreEtat_fk = ? WHERE NumExemplaire = ? AND NomJeu_fk = ?";
        PreparedStatement statement = connection.prepareStatement(insertGameSql);
        
        statement.setString(1, etatCommentaire);
        statement.setString(2, titreEtat);
        statement.setInt(3, numExemplaire);
        statement.setString(4, nomJeuFk);
        
        statement.executeUpdate();
        return true;
    }

    public boolean deleteSampleGame(String titre, int num) throws SQLException {
        String deleteGameSql = "DELETE FROM Exemplaire WHERE NomJeu_fk = ? AND NumExemplaire = ?";
        PreparedStatement statement = connection.prepareStatement(deleteGameSql);
        
        statement.setString(1, titre);
        statement.setInt(2, num);
        
        statement.executeUpdate();
        return true;
    }
    
    public void deletePersonne(String pseudo) throws SQLException {
        //Suppression de la personne
        String deleteGameSql = "DELETE FROM Personne WHERE Pseudo = ?";
        PreparedStatement statement = connection.prepareStatement(deleteGameSql);
        statement.setString(1, pseudo);
        statement.executeUpdate();
    }
    
    public boolean deleteEmployee(String pseudo) throws SQLException {
        //Recupère l'id
        PreparedStatement state = connection.prepareStatement("SELECT Id FROM personne WHERE Pseudo = ?");
        state.setString(1, pseudo);
        ResultSet set = state.executeQuery();
        if (!set.next()) //Pas de personne associée au pseudo
        {
            return false;
        }
        
        int id = set.getInt("Id");
        if (!exists("Employe", "Id_fk", id)) //Pas d'employé associé au pseudo
        {
            return false;
        }

        //Suppression de l'employe
        String deleteGameSql = "DELETE FROM Employe WHERE Id_fk = ?";
        PreparedStatement statement = connection.prepareStatement(deleteGameSql);
        statement.setInt(1, id);
        statement.executeUpdate();
        
        deletePersonne(pseudo);
        return true;
    }
    
        public boolean extendLoan(int idChoice) throws SQLException {
        
        try {
            String extendLoanSql = "UPDATE emprunt SET NombreProlongation = NombreProlongation+1 WHERE Id = ?";
            PreparedStatement statement = connection.prepareStatement(extendLoanSql);
            statement.setInt(1, idChoice);
            statement.executeUpdate();
        } catch (SQLException e) {
            return false;
        }
        
        return true;        
    }
}
