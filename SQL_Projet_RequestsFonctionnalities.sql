USE JAGM;

-- Fonctionnalites application

-- quelque insertions pour tester:
INSERT INTO Droit (Titre) VALUES ('Admin');

INSERT INTO Personne (
    Nom,
    Prenom,
    Pseudo,
    MotDePasse,
    Age,
    AdrNPA,
    AdrRue,
    AdrVille,
    Mail,
    NumTel,
    EstSupprime,
    DateSuppression,
    DateInscription,
    DroitTitre_fk)
    VALUES
    ('nom', 'prenom', 'pseudo', 'mdp', 24, 1052, 'ch.des Piécettes 6', 'Mont-sur-Lausanne',
    'nom.prenom@mail.com', '045/566.23.22', false, NULL, current_date(), 'Admin');

INSERT INTO membre (CarteIdentite,
    NbEmpruntEnCours,
    NbReservationEnCours,
    Abonne,
    Id_fk) 
    VALUES 
    ('moi.pdf', 0, 0, true, (SELECT Id FROM Personne WHERE Nom = 'nom' AND Prenom = 'prenom') );
    
INSERT INTO Etat (Titre) VALUES ("OK");

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("XBOX360", 2015);
 
INSERT INTO Editeur (Nom) VALUES ("editeur1");
 
INSERT INTO Genre (Titre) VALUES ("aventure");
 
INSERT INTO Jeu (Titre,
    AnneeProduction,
    LimiteAge,    
    NomConsole_fk,
    NomEditeur_fk,
    GenreTitre_fk) 
    VALUES ("Last Guardian", 2016, 18, "XBOX360", "editeur1", "aventure");
    
INSERT INTO Magasin (NPA,
    Ville,
    Rue,
    NumRue) 
    VALUES (1052, "Mont-sur-Lausanne", "Piécettes", 6);

INSERT INTO Exemplaire (
    NumExemplaire,
	EtatCommentaire,
    TitreEtat_fk,
    NPA_fk,
    NomConsole_fk,
    NomJeu_fk) 
	VALUES (1, "bon", "OK", 1052, "XBOX360", "Last Guardian");

-- Reservation de jeux
INSERT INTO Reservation ( -- la quantite limitee sera verifiee par le code du site internet avant l'insertion
	DateReservation,
    DateRetraitSouhaite,
    IdMembre_fk,
	IdExemplaire_fk,
    NumExemplaire_fk) VALUES
    (current_date(), DATE_ADD(DateReservation,INTERVAL 10 DAY), 7,41,2); -- attention au format de la date: annee-mois-jour. le 10 dit dans combien de jour on veut le retirer
-- select * from reservation;

-- Recupere un exemplaire a partir du nom
SELECT Id, NumExemplaire 
FROM Exemplaire
WHERE NomJeu_fk = "Super Mario Bros." AND NomConsole_fk = "NES";

INSERT INTO Emprunt (
    DateEmprunt,
    DureeJour,
    NombreProlongation,
    IdMembre_fk,
    IdExemplaire_fk,
    NumExemplaire_fk)
    VALUES (current_date(), 10, 0, 7, 42, 3);
    
DELETE FROM emprunt WHERE id = 20;
    
    
select * from Emprunt;
select * from emprunt_log;

-- vue pour les emprunts en retard, permettra d'afficher une page pour gerer les retards (penalite)
DROP VIEW IF EXISTS emprunt_retard;
CREATE VIEW emprunt_retard
 AS
	SELECT
        membre.CarteIdentite,
        emprunt.Id,
		emprunt.DateEmprunt,
        emprunt.DureeJour,
        DATE_ADD(emprunt.DateEmprunt, INTERVAL emprunt.DureeJour DAY) as dateRetour,
        DATEDIFF(CURDATE(), DATE_ADD(emprunt.DateEmprunt, INTERVAL emprunt.DureeJour DAY)) as nbJoursRetard,
        membre.NbEmpruntEnCours
    FROM membre
    INNER JOIN emprunt
		ON emprunt.IdMembre_fk = membre.Id_fk
	HAVING nbJoursRetard  > 0;
        
SELECT * FROM emprunt_retard;


-- Gestion stockage: trouver les magasins ou retirer l'exemplaire souhaite
SELECT NPA,
    Ville,
    Rue,
    NumRue
    FROM magasin
    INNER JOIN Exemplaire
    ON Exemplaire.NPA_fk = magasin.NPA
    WHERE exemplaire.Id = 1 AND exemplaire.NumExemplaire = 1; -- 1 est le numero de l'exemplaire selectionne via le site.

-- Jeux et consoles
-- systeme notation, insertion et affichage trie

INSERT INTO Avis (
    Note,
    Commentaire,
    JeuTitre_fk) 
    VALUES 
    (3, "Tres sympa", "Last Guardian");


SELECT *
FROM Avis
ORDER BY Note DESC;

-- Affichage trie par editeur
SELECT * 
FROM Jeu
INNER JOIN Editeur
ON Jeu.NomEditeur_fk = Editeur.Nom
ORDER BY Editeur.Nom ASC;

-- par titre de jeu (alphabetique)
SELECT *
FROM Jeu
ORDER BY Titre ASC;

-- par genre (alphabetique)
SELECT *
FROM Jeu
INNER JOIN Genre
ON Genre.Titre = Jeu.GenreTitre_fk
ORDER BY Genre.Titre ASC;

-- par note (moyenne)
SELECT *, AVG(Avis.Note) AS NoteMoyenne
FROM Jeu
INNER JOIN Avis
ON Avis.JeuTitre_fk = Jeu.Titre
ORDER BY NoteMoyenne DESC;


-- Etat des jeux amende
SELECT *
FROM Exemplaire
WHERE TitreEtat_fk = "mauvais"; -- mauvais a mettre dans protocole d'insertion

-- selection jeu selon son nom
SELECT *
FROM Jeu
WHERE Jeu.Titre = "Last Guardian";

-- selection console selon nom
SELECT * 
FROM Console
WHERE Console.Nom = "XBOX360";

-- selection membre selon sa carte d'identite
SELECT *
FROM membre
WHERE membre.CarteIdentite = "moi.pdf";

-- Membre
-- gestion profil, modification uniquement par gestionnaire (verifier droit via site)
UPDATE membre
SET CarteIdentite = "moi.pdf",
    NbEmpruntEnCours = 0,
    NbReservationEnCours = 0,
    Abonne = true
WHERE Id_fk = 0;

-- gestion emprunt et reservation
UPDATE Emprunt
SET
    DateEmprunt = current_date(),
    DureeJour = 10,
    NombreProlongation = 2
WHERE IdMembre_fk = 1 AND IdExemplaire_fk = 1 AND NumExemplaire_fk = 1;

-- gestion reservation
UPDATE reservation 
SET DateReservation = current_date(),
    DateRetraitSouhaite = DATE_ADD(DateReservation,INTERVAL 10 DAY)    
WHERE IdMembre_fk = 1 AND
	IdExemplaire_fk = 1 AND
    NumExemplaire_fk = 1;
    
-- Liste emprunt par membre
SELECT *
FROM emprunt
INNER JOIN membre
ON membre.Id_fk = emprunt.IdMembre_fk AND membre.CarteIdentite = "moi.pdf";

-- liste reservation par membre
SELECT *
FROM reservation
INNER JOIN membre
ON membre.Id_fk = reservation.IdMembre_fk AND membre.CarteIdentite = "moi.pdf";

-- suppression reservation
DELETE FROM reservation
WHERE reservation.IdMembre_fk = 7 AND reservation.IdExemplaire_fk = 41;

-- Employe
-- suppression membre: uniquement via le booleen
UPDATE Personne 
SET Personne.EstSupprime = true
WHERE Personne.Nom = "nom" AND Personne.Prenom = "prenom"; -- l'affichage fera en sorte de trier les personnes supprimees

SELECT *
FROM personne
WHERE EstSupprime = false;

-- Gestion stock
-- suppression jeu
DELETE FROM Avis
WHERE Avis.JeuTitre_fk = "Last Guardian";
DELETE FROM Exemplaire
WHERE Exemplaire.NomJeu_fk = "Last Guardian";
DELETE FROM Jeu
WHERE Jeu.Titre = "Last Guardian";

-- modification jeu
UPDATE Jeu
SET AnneeProduction = 2015,
    LimiteAge = 18,    
    NomConsole_fk = "XBOX360",
    NomEditeur_fk = "editeur1",
    GenreTitre_fk = "aventure"
WHERE Titre = "Last Guardian";

-- modification console
UPDATE console
SET AnneeProduction = 2015
WHERE Nom = "XBOX360";
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
