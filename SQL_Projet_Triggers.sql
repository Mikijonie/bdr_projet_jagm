USE jagm;
SET GLOBAL event_scheduler = ON;

-- Création de la carte d'indetité d'un User

DROP TRIGGER IF EXISTS after_insert_new_member;
DELIMITER $$
CREATE TRIGGER after_insert_new_member
BEFORE INSERT
ON membre
FOR EACH ROW
	BEGIN
		-- Création de la carte d'indetité
		SET	NEW.CarteIdentite = CONCAT((SELECT Nom FROM Personne WHERE NEW.id_fk = Personne.Id), ".pdf");
	END $$
DELIMITER ;

-- RESERVATION

DROP TRIGGER IF EXISTS after_reservation_insertion;
DELIMITER $$
CREATE TRIGGER after_reservation_insertion
AFTER INSERT
ON Reservation FOR EACH ROW
BEGIN
	-- Mise à jour du nombre de réservation du membre
	UPDATE Membre 
    SET NbReservationEnCours = NbReservationEnCours + 1
    WHERE Membre.Id_fk = NEW.IdMembre_fk;
END $$
DELIMITER ;

-- Emprunt

DROP TRIGGER IF EXISTS after_emprunt_insertion;
DELIMITER $$
CREATE TRIGGER after_emprunt_insertion
AFTER INSERT
ON Emprunt FOR EACH ROW
BEGIN
	-- Mise à jour du nombre d'emprunt du membre
	UPDATE Membre 
    SET NbEmpruntEnCours = NbEmpruntEnCours + 1
    WHERE Membre.Id_fk = NEW.IdMembre_fk;
    
    -- Ajout de l'emprunt à la liste d'emprunt du membre
    INSERT INTO Emprunt_log (IdMembre_fk, NomConsole_fk, NomJeu_fk)
		SELECT 
			NEW.IdMembre_fk, 
            NomConsole_fk, 
            NomJeu_fk
		FROM Exemplaire
        WHERE Exemplaire.Id = NEW.IdExemplaire_fk;    
	
END $$
DELIMITER ;


DROP TRIGGER IF EXISTS after_reservation_delete;
DELIMITER $$
CREATE TRIGGER after_reservation_delete
AFTER DELETE
ON Reservation FOR EACH ROW
BEGIN
	-- Mise à jour du nombre de réservation du membre
	UPDATE Membre 
    SET NbReservationEnCours = NbReservationEnCours - 1
    WHERE Membre.Id_fk = OLD.IdMembre_fk;
END $$
DELIMITER ;

DROP TRIGGER IF EXISTS after_emprunt_delete;
DELIMITER $$
CREATE TRIGGER after_emprunt_delete
AFTER DELETE
ON Emprunt FOR EACH ROW
BEGIN
	-- Mise à jour du nombre d'emprunt du membre
	UPDATE Membre 
    SET NbEmpruntEnCours = NbEmpruntEnCours - 1
    WHERE Membre.Id_fk = OLD.IdMembre_fk;
    
END $$
DELIMITER ;

DROP EVENT IF EXISTS amendes_MAJ;
CREATE EVENT amendes_MAJ
ON SCHEDULE EVERY 1 DAY
DO 
	CALL MAJ_amendes();
