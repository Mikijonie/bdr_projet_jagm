--
-- Database: JAGM
-- SQL_JAGM.sql
--

-- -------

DROP SCHEMA IF EXISTS JAGM;

CREATE SCHEMA JAGM;

USE JAGM;
-- ############################################################## MAGASIN ##########################################################################

-- --------------------------------------------------------
--
-- Table structure for table Magasin
--

CREATE TABLE Magasin (
	NPA int(4) NOT NULL,
    Ville varchar(30) NOT NULL,
    Rue varchar(50) NOT NULL,
    NumRue int(4) NOT NULL,
    
    PRIMARY KEY (NPA)
);

-- ############################################################# PERSONNE #########################################################################

-- --------------------------------------------------------
--
-- Table structure for table Droit
--

CREATE TABLE Droit (
	Titre varchar(20) NOT NULL,
    
    PRIMARY KEY (Titre)     
);

-- --------------------------------------------------------
--
-- Table structure for table Personne
--

CREATE TABLE Personne (
	Id int(10) NOT NULL AUTO_INCREMENT,
    Nom varchar(30) NOT NULL,
    Prenom varchar(30) NOT NULL,
    Pseudo varchar(30) NOT NULL,
    MotDePasse varchar(100) NOT NULL,
    Age int(3) NOT NULL,
    AdrNPA int(4) NOT NULL,
    AdrRue varchar(50) NOT NULL,
    AdrVille varchar(30) NOT NULL,
    Mail varchar(70) NOT NULL,
    NumTel varchar(15) NOT NULL,
    EstSupprime bool NOT NULL DEFAULT false,
    DateSuppression datetime,
    DateInscription datetime,
    DroitTitre_fk varchar(20) DEFAULT "User",
    
    PRIMARY KEY (Id),
    FOREIGN KEY (DroitTitre_fk) REFERENCES Droit (Titre) ON UPDATE CASCADE 
);

-- --------------------------------------------------------
--
-- Table structure for table Employe
--

CREATE TABLE Employe (
	Salaire decimal(20,2) NOT NULL,
	NPA_fk int(4) NOT NULL,
	Id_fk int(10) NOT NULL,
	
	PRIMARY KEY (Id_fk),
	FOREIGN KEY (Id_fk) REFERENCES Personne (Id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (NPA_fk) REFERENCES Magasin (NPA) ON UPDATE CASCADE
);
   
-- --------------------------------------------------------
--
-- Table structure for table Membre
--

CREATE TABLE Membre (
	CarteIdentite varchar(50),
    NbEmpruntEnCours int(2) NOT NULL DEFAULT 0,
    NbReservationEnCours int(2) NOT NULL DEFAULT 0,
    Abonne bool NOT NULL DEFAULT false,
    
	Id_fk int(10),
	
	PRIMARY KEY (Id_fk),
	FOREIGN KEY (Id_fk) REFERENCES Personne (Id) ON DELETE CASCADE ON UPDATE CASCADE 
);

-- ############################################################## CONSOLE ####################################################################

-- --------------------------------------------------------
--
-- Table structure for table Console
--

CREATE TABLE Console (
	Nom varchar(20) NOT NULL,
    AnneeProduction year,
    
    PRIMARY KEY (Nom)
);

-- ############################################################## JEU ##########################################################################

-- --------------------------------------------------------
--
-- Table structure for table Editeur
--

CREATE TABLE Editeur (
	Nom varchar(30) NOT NULL,
    
    PRIMARY KEY (Nom)     
);

-- --------------------------------------------------------
--
-- Table structure for table Genre
--

CREATE TABLE Genre (
	Titre varchar(20) NOT NULL,
    
    PRIMARY KEY (Titre)
);

-- --------------------------------------------------------
--
-- Table structure for table Jeu
--

CREATE TABLE Jeu (
	Titre varchar(50) NOT NULL,
    AnneeProduction year NOT NULL,
    LimiteAge int(3),    
    NomConsole_fk varchar(20) NOT NULL,
    NomEditeur_fk varchar(30) NOT NULL,
    GenreTitre_fk varchar(20) NOT NULL,
    
    PRIMARY KEY (Titre),
    FOREIGN KEY (NomConsole_fk) REFERENCES Console (Nom) ON UPDATE CASCADE,
    FOREIGN KEY (NomEditeur_fk) REFERENCES Editeur (Nom) ON UPDATE CASCADE,     
    FOREIGN KEY (GenreTitre_fk) REFERENCES Genre (Titre) ON UPDATE CASCADE
);

-- --------------------------------------------------------
--
-- Table structure for table Avis
--

CREATE TABLE Avis (
	Id int(10) NOT NULL AUTO_INCREMENT,
    Note int(2) NOT NULL,
    Commentaire varchar(200),
    JeuTitre_fk varchar(50) NOT NULL,
    
    PRIMARY KEY (Id),
    FOREIGN KEY (JeuTitre_fk) REFERENCES Jeu (Titre) ON UPDATE CASCADE ON DELETE CASCADE
);

-- ############################################################## EXEMPLAIRE ##################################################################

-- --------------------------------------------------------
--
-- Table structure for table Etat
--

CREATE TABLE Etat (
	Titre varchar(10) NOT NULL,
    
	PRIMARY KEY (Titre)
);

-- --------------------------------------------------------
--
-- Table structure for table Exemplaire
--

CREATE TABLE Exemplaire (
	Id int(10) NOT NULL AUTO_INCREMENT,
    NumExemplaire int(10) NOT NULL,
	EtatCommentaire varchar(100),
    TitreEtat_fk varchar(10),
    NPA_fk int(4) NOT NULL,
    NomConsole_fk varchar(20),
    NomJeu_fk varchar(50),
    
    
    PRIMARY KEY (Id, NumExemplaire),
    FOREIGN KEY (NPA_fk) REFERENCES Magasin (NPA),
    FOREIGN KEY (TitreEtat_fk) REFERENCES Etat (Titre),
    FOREIGN KEY (NomConsole_fk) REFERENCES Console (Nom),
    FOREIGN KEY (NomJeu_fk) REFERENCES Jeu (Titre)
);

-- ####################################################### EMPRUNT ET RESERVATION ###################################################################

-- --------------------------------------------------------
--
-- Table structure for table Emprunt
--

CREATE TABLE Emprunt (
	Id int(10) NOT NULL AUTO_INCREMENT,
    DateEmprunt date NOT NULL,
    DureeJour int(2) NOT NULL,
    NombreProlongation int(2) NOT NULL DEFAULT '0',
    IdMembre_fk int(10) NOT NULL,
    IdExemplaire_fk int(10) NOT NULL,
    NumExemplaire_fk int(10) NOT NULL, 
    
    PRIMARY KEY (Id),    
    FOREIGN KEY (IdMembre_fk) REFERENCES Membre (Id_fk) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (IdExemplaire_fk, NumExemplaire_fk) REFERENCES Exemplaire (Id, NumExemplaire) ON UPDATE CASCADE ON DELETE CASCADE
);

-- --------------------------------------------------------
--
-- Table structure for table Reservation
--

CREATE TABLE Reservation (
	Id int(10) NOT NULL AUTO_INCREMENT,
    DateReservation date NOT NULL,
    DateRetraitSouhaite date NOT NULL,
    IdMembre_fk int(10) NOT NULL,
	IdExemplaire_fk int(10) NOT NULL,
    NumExemplaire_fk int(10) NOT NULL, 
    
    PRIMARY KEY (Id),
    FOREIGN KEY (IdMembre_fk) REFERENCES Membre (Id_fk) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (IdExemplaire_fk, NumExemplaire_fk) REFERENCES Exemplaire (Id, NumExemplaire) ON UPDATE CASCADE ON DELETE CASCADE
);

-- ############################################################## AMENDE #######################################################################

-- --------------------------------------------------------
--
-- Table structure for table TypeAmende
--

CREATE TABLE TypeAmende (
	Nom varchar(20) NOT NULL,
    PrixJournee decimal(10,2),
    MontantUnique decimal(10,2),
    
    PRIMARY KEY (Nom)     
);

-- --------------------------------------------------------
--
-- Table structure for table Amende
--

CREATE TABLE Amende (
	Id int(10) NOT NULL AUTO_INCREMENT,
    DateInitial date NOT NULL,
    TypeAmende_fk varchar(10) NOT NULL,
    EmpruntId_fk int(10) NOT NULL,
    
    PRIMARY KEY (Id), 
    FOREIGN KEY (TypeAmende_fk) REFERENCES TypeAmende (Nom) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (EmpruntId_fk) REFERENCES Emprunt (Id) ON UPDATE CASCADE ON DELETE CASCADE
);

-- ############################################################## LOG #######################################################################

-- --------------------------------------------------------
--
-- Table structure for table Emprunt_log
--

CREATE TABLE Emprunt_log (
	IdMembre_fk int(10) NOT NULL,
    NomConsole_fk varchar(20),
    NomJeu_fk varchar(50)
);