USE JAGM;

/*
FAIT :
Vue sur son profile comme membre
Vue sur son profile comme admin
Vue sur le stock de jeux
Vue sur le stock de console
Vue sur le stock de jeux disponibles
Vue sur le stock de consoles disponibles
Vue sur les avis
Vue sur la liste des employés
Vue filtrée sur la liste des employés
Vue sur la liste des membres
Vue filtrée sur la liste des membres
Vue sur tout les emprunts
Vue sur les emprunts en retard
Vue sur les membres qui ne sont pas abonné
Vue sur les membres supprimés
Vue sur toutes les réservations
Vue sur toutes les réservation d'un seul user
Vue sur les emprunts d'un seul user
Vue sur les employés d'un magasin
Vue sur toutes les amendes (CREER INSERTION POUR TEST)
Vue sur les amendes d'un seul user
Vue sur ton profile
*/
  
-- Voir son profile comme membre
DROP VIEW IF EXISTS voir_profile_membre;
CREATE VIEW voir_profile_membre
AS
	SELECT Nom, Prenom, Pseudo, Age, AdrRue, AdrVille, AdrNPA, Mail, NumTel, CarteIdentite, NbEmpruntEnCours, NbReservationEnCours
    FROM Personne
		INNER JOIN Membre
		ON Personne.Id = Membre.Id_fk
	GROUP BY Personne.Id;

-- Voir son profile comme admin
DROP VIEW IF EXISTS voir_profile_employe;
CREATE VIEW voir_profile_employe
AS
	SELECT Nom, Prenom, Pseudo, Age, AdrRue, AdrVille, AdrNPA, Mail, NumTel, Salaire
    FROM Personne
		INNER JOIN Employe
		ON Personne.Id = Employe.Id_fk
	GROUP BY Personne.Id;

-- Voir les exemplaires de consoles
DROP VIEW IF EXISTS stock_exemplaires_consoles;
CREATE VIEW stock_exemplaires_consoles
AS
	SELECT *
    FROM exemplaire
    WHERE (exemplaire.NomJeu_fk IS NULL);
    
-- Voir les exemplaires de jeu
DROP VIEW IF EXISTS stock_exemplaires_jeu;
CREATE VIEW stock_exemplaires_jeu
AS
	SELECT *
    FROM exemplaire
    WHERE NOT(exemplaire.NomJeu_fk IS NULL);
        
-- Voir si les exemplaires de jeux libres
DROP VIEW IF EXISTS jeux_disponibles;
CREATE VIEW jeux_disponibles
AS
	SELECT *, COUNT(*) AS nbr_ex_jeux
    FROM stock_exemplaires_jeu
    WHERE
		(stock_exemplaires_jeu.Id NOT IN (
			SELECT emprunt.IdExemplaire_fk
			FROM emprunt)
		)
        AND
        (stock_exemplaires_jeu.Id NOT IN (
			SELECT reservation.IdExemplaire_fk
            FROM reservation)
		)
	GROUP BY stock_exemplaires_jeu.NomJeu_fk, stock_exemplaires_jeu.NomConsole_fk;
    
-- Voir si les exemplaires de jeux libres
DROP VIEW IF EXISTS jeux_disponibles_avec_Ex;
CREATE VIEW jeux_disponibles_avec_Ex
AS
	SELECT *
    FROM stock_exemplaires_jeu
    WHERE
		(stock_exemplaires_jeu.Id NOT IN (
			SELECT emprunt.IdExemplaire_fk
			FROM emprunt)
		)
        AND
        (stock_exemplaires_jeu.Id NOT IN (
			SELECT reservation.IdExemplaire_fk
            FROM reservation)
		);

-- Voir les exemplaires de consoles libres
DROP VIEW IF EXISTS consoles_disponibles;
CREATE VIEW consoles_disponibles
AS
	SELECT *, COUNT(*) AS nbr_ex_consoles
    FROM stock_exemplaires_consoles
    WHERE (stock_exemplaires_consoles.Id NOT IN (
		SELECT emprunt.IdExemplaire_fk
        FROM emprunt))
        AND (stock_exemplaires_consoles.Id NOT IN (
		SELECT reservation.IdExemplaire_fk
		FROM reservation))
	GROUP BY stock_exemplaires_consoles.NomConsole_fk;

-- Voir tous les avis
DROP VIEW IF EXISTS liste_avis;
CREATE VIEW liste_avis
AS
	SELECT Note, Commentaire, JeuTitre_fk
	FROM Avis
		INNER JOIN Jeu
		ON Avis.JeuTitre_fk = Jeu.Titre
    GROUP BY Avis.Id;

-- Liste des employés
DROP VIEW IF EXISTS liste_employes;
CREATE VIEW liste_employes
AS
	SELECT *
	FROM personne
    INNER JOIN employe
		ON personne.Id = employe.Id_fk
    WHERE personne.Id IN (
		SELECT employe.Id_fk
        FROM employe);
        
-- Liste des employés filtrée (Affiche que les employés non supprimés. Leur Nom complet, pseudo et lieu de travail)
DROP VIEW IF EXISTS liste_employes_filtre;
CREATE VIEW liste_employes_filtre
AS
	SELECT
        liste_employes.Prenom,
		liste_employes.Nom,
        liste_employes.Pseudo,
        liste_employes.NPA_fk
	FROM liste_employes
    WHERE liste_employes.EstSupprime = 0;
    
-- Liste des membres
DROP VIEW IF EXISTS liste_membres;
CREATE VIEW liste_membres
AS
	SELECT *
	FROM personne
    INNER JOIN membre
		ON personne.Id = membre.Id_fk
    WHERE personne.Id IN (
		SELECT membre.Id_fk
        FROM membre)
        AND personne.EstSupprime = false;
        
-- Liste des membres filtrées
DROP VIEW IF EXISTS liste_membres_filtre;
CREATE VIEW liste_membres_filtre
AS
	SELECT personne.Pseudo, personne.DateInscription
	FROM personne
    INNER JOIN membre
		ON personne.Id = membre.Id_fk
    WHERE personne.Id IN (
		SELECT membre.Id_fk
        FROM membre)
        AND personne.EstSupprime = false;


-- Liste des emprunts
DROP VIEW IF EXISTS liste_emprunts;
CREATE VIEW liste_emprunts
AS
	SELECT 
        emprunt.Id,
        emprunt.DateEmprunt,
        emprunt.DureeJour,
        emprunt.NombreProlongation,
        emprunt.IdMembre_fk,
        liste_membres.Pseudo,
		exemplaire.Id AS Id_Exemplaire,
        exemplaire.NumExemplaire,
        exemplaire.EtatCommentaire,
        exemplaire.TitreEtat_fk,
        exemplaire.NPA_fk,
        exemplaire.NomConsole_fk,
        exemplaire.NomJeu_fk
	FROM emprunt
    INNER JOIN exemplaire
		ON emprunt.IdExemplaire_fk = exemplaire.Id
	INNER JOIN liste_membres
		ON emprunt.IdMembre_fk = liste_membres.Id_fk
	WHERE exemplaire.Id IN (
		SELECT emprunt.IdExemplaire_fk
        FROM emprunt)
	ORDER BY emprunt.ID ASC;
    
-- Liste des emprunts avec date de reddition
DROP VIEW IF EXISTS liste_emprunts_date;
CREATE VIEW liste_emprunts_date
AS
	SELECT 
        *,
        DATE_ADD(DateEmprunt, INTERVAL (DureeJour * (NombreProlongation+1)) DAY) AS date_fin       
	FROM liste_emprunts;

-- Liste des réservations
DROP VIEW IF EXISTS liste_reservation;
CREATE VIEW liste_reservation
AS
	SELECT 
		reservation.Id,
        reservation.DateReservation,
        reservation.DateRetraitSouhaite,
        reservation.IdMembre_fk,
        liste_membres.Pseudo,
        exemplaire.Id AS Id_Exemplaire,
        exemplaire.NumExemplaire,
        exemplaire.EtatCommentaire,
        exemplaire.TitreEtat_fk,
        exemplaire.NPA_fk,
        exemplaire.NomConsole_fk,
        exemplaire.NomJeu_fk
        
	FROM reservation
    INNER JOIN exemplaire
		ON exemplaire.Id = reservation.IdExemplaire_fk
	INNER JOIN liste_membres
		ON reservation.IdMembre_fk = liste_membres.Id_fk
	WHERE exemplaire.Id IN (
		SELECT reservation.IdExemplaire_fk
        FROM reservation);

-- Liste des emprunts en retard
DROP VIEW IF EXISTS retards;
CREATE VIEW retards
AS
	SELECT *, ( DATEDIFF(CURRENT_DATE(), DATE_ADD(liste_emprunts.DateEmprunt, INTERVAL liste_emprunts.DureeJour * liste_emprunts.NombreProlongation DAY))) AS day_late
    FROM liste_emprunts
    HAVING day_late > 0 ;
    
-- vue pour les emprunts en retard, permettra d'afficher une page pour gerer les retards (penalite)
DROP VIEW IF EXISTS emprunt_retard_membre;
CREATE VIEW emprunt_retard_membre
 AS
	SELECT
        membre.CarteIdentite,
        emprunt.Id,
		emprunt.DateEmprunt,
        emprunt.DureeJour,
        DATE_ADD(emprunt.DateEmprunt, INTERVAL emprunt.DureeJour DAY) as dateRetour,
        DATEDIFF(CURDATE(), DATE_ADD(emprunt.DateEmprunt, INTERVAL emprunt.DureeJour DAY)) as nbJoursRetard,
        membre.NbEmpruntEnCours
    FROM membre
    INNER JOIN emprunt
		ON emprunt.IdMembre_fk = membre.Id_fk
	HAVING nbJoursRetard  > 0;

-- Liste des membres non abonné
DROP VIEW IF EXISTS membres_non_abo;
CREATE VIEW membres_non_abo
AS
	SELECT *
	FROM liste_membres
    WHERE liste_membres.Abonne = false AND liste_membres.EstSupprime = false;
    
-- Liste des membres supprimés
DROP VIEW IF EXISTS membres_supprimes;
CREATE VIEW membres_supprimes
AS
	SELECT *
	FROM liste_membres
    WHERE liste_membres.EstSupprime = true;
    
-- Liste de amendes
DROP VIEW IF EXISTS liste_amendes;
CREATE VIEW liste_amendes
AS
	SELECT 
		amende.DateInitial,
		amende.EmpruntId_fk,
        amende.Id AS Amende_id,
        amende.TypeAmende_fk,
        typeamende.MontantUnique,
        typeamende.PrixJournee,
        emprunt.*,
        personne.Id AS personne_id,
        personne.Nom,
        personne.Prenom,
        personne.Pseudo,
        exemplaire.NomJeu_fk,
        exemplaire.NomConsole_fk,
        DATEDIFF(NOW(), DateInitial) AS Jours,
        MontantUnique + DATEDIFF(NOW(), DateInitial) * PrixJournee AS PrixTotal
	FROM amende
    INNER JOIN emprunt
		ON emprunt.Id = EmpruntId_fk
	INNER JOIN personne
		ON emprunt.IdMembre_fk = personne.Id
	INNER JOIN exemplaire
		ON emprunt.IdExemplaire_fk = Exemplaire.Id AND
        emprunt.NumExemplaire_fk = Exemplaire.NumExemplaire
	INNER JOIN TypeAmende
		ON TypeAmende.nom = amende.TypeAmende_fk;
    
SELECT * FROM liste_amendes;

-- ############################################################## FONCTIONS ########################################################################

-- Liste des réservation d'un seul membre
DROP PROCEDURE IF EXISTS reservation_un_membre;
DELIMITER $$
CREATE PROCEDURE reservation_un_membre (id_membre INT)
BEGIN
	SELECT *
    FROM liste_reservation
    INNER JOIN
    (
		SELECT
			liste_membres.Id AS Id_Membre,
            liste_membres.Pseudo
		FROM liste_membres
    
	) liste ON liste.Id_Membre = liste_reservation.IdMembre_fk
    WHERE liste.Id_Membre = id_membre;
END $$
DELIMITER ;

-- Liste des emprunts d'un seul membre
DROP PROCEDURE IF EXISTS emprunts_un_membre;
DELIMITER $$
CREATE PROCEDURE emprunts_un_membre (id_membre INT)
BEGIN
	SELECT *
    FROM liste_emprunts
    INNER JOIN
    (
		SELECT
			liste_membres.Id AS Id_Membre,
            liste_membres.Pseudo
		FROM liste_membres
    
	) liste ON liste.Id_Membre = liste_emprunts.IdMembre_fk
    WHERE liste.Id_Membre = id_membre;
END $$
DELIMITER ;

-- Liste des amende d'un seul membre
DROP PROCEDURE IF EXISTS amende_un_membre;
DELIMITER $$
CREATE PROCEDURE amende_un_membre (id_membre INT)
BEGIN
	SELECT *
    FROM liste_amende
    WHERE liste.Id_Membre = id_membre;
END $$
DELIMITER ;

-- Ajout/Mise à jour des amendes d'un emprunt spécifique
-- A appeler 1 fois par jour sur tous les emprunts
DROP PROCEDURE IF EXISTS MAJ_amende_retard;
DELIMITER $$
CREATE PROCEDURE MAJ_amende_retard (IN id_emprunt INT)
BEGIN 

    IF EXISTS (
		SELECT retards.Id
		FROM retards
		WHERE Id = id_emprunt) 
	THEN -- On vérifie qu'il y a un retard sur l'emprunt
		
        IF NOT EXISTS (
			SELECT amende.EmpruntId_fk
            FROM amende
            WHERE
				amende.EmpruntId_fk = id_emprunt AND
                amende.TypeAmende_fk = "Retard" ) 
		THEN -- On vérifie que l'amende n'est pas déjà présente
		
			INSERT INTO amende(DateInitial, EmpruntId_fk, TypeAmende_fk)
				VALUES (current_date(), id_emprunt, "Retard");
		END IF;
        
    END IF;
END $$
DELIMITER ;

-- Met a jour toutes les amendes
-- Basé sur http://stackoverflow.com/questions/14326775/call-a-stored-procedure-for-each-row-returned-by-a-query-in-mysql
DROP PROCEDURE IF EXISTS MAJ_amendes;
DELIMITER $$
CREATE PROCEDURE MAJ_amendes() BEGIN
  DECLARE done BOOLEAN DEFAULT FALSE;
  DECLARE _id BIGINT UNSIGNED;
  DECLARE cur CURSOR FOR SELECT Id FROM retards;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done := TRUE;

  OPEN cur;

  testLoop: LOOP
    FETCH cur INTO _id;
    IF done THEN
      LEAVE testLoop;
    END IF;
    CALL MAJ_amende_retard(_id);
  END LOOP testLoop;

  CLOSE cur;
END $$
DELIMITER ;