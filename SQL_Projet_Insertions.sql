USE JAGM;

-- ############################################################## MAGASINS ##########################################################################

INSERT INTO magasin(NPA,
				 Ville,
				 Rue,
				 NumRue)
	VALUES (1018, "Lausanne", "Rue de Genève", 20);

-- ############################################################## DROITS ##########################################################################

INSERT INTO Droit (Titre) VALUES ("Admin");
INSERT INTO Droit (Titre) VALUES ("User");

-- ############################################################# PERSONNES #########################################################################
INSERT INTO Personne (Nom,
					Prenom,
					Pseudo,
					MotDePasse,
					Age,
					AdrNPA,
					AdrRue,
					AdrVille,
					Mail,
					NumTel,
					EstSupprime,
					DateSuppression,
					DateInscription,
					DroitTitre_fk)
	VALUES ("root",
			"root",
			"root",
			"root",
			22,
			6669,
			"Avenue de la Méchanceté 666",
			"Grand Méchant",
			"super.mechant@hell.no",
			"069/666.69.69",
			false,
			null,
			current_date(),
			"Admin");
            
INSERT INTO Personne (Nom,
		Prenom,
		Pseudo,
		MotDePasse,
		Age,
		AdrNPA,
		AdrRue,
		AdrVille,
		Mail,
		NumTel,
		EstSupprime,
		DateSuppression,
		DateInscription,
					DroitTitre_fk)
	VALUES ("user",
			"user",
			"user",
			"user",
			22,
			6669,
			"Avenue de la Méchanceté 666",
			"Grand Méchant",
			"super.mechant@hell.no",
			"069/666.69.69",
			false,
			null,
			current_date(),
			"User");
            
INSERT INTO Personne (Nom,
					Prenom,
					Pseudo,
					MotDePasse,
					Age,
					AdrNPA,
					AdrRue,
					AdrVille,
					Mail,
					NumTel,
					EstSupprime,
					DateSuppression,
					DateInscription,
					DroitTitre_fk)
	VALUES ("Pas",
			"Nathan",
			"nat",
			"Les patates c'est bien",
			22,
			6669,
			"Avenue de la Méchanceté 666",
			"Grand Méchant",
			"super.mechant@hell.no",
			"069/666.69.69",
			false,
			null,
			current_date(),
			"Admin");
            
INSERT INTO Personne (Nom,
					Prenom,
					Pseudo,
					MotDePasse,
					Age,
					AdrNPA,
					AdrRue,
					AdrVille,
					Mail,
					NumTel,
					EstSupprime,
					DateSuppression,
					DateInscription,
					DroitTitre_fk)
	VALUES ("Dofa",
			"Rémi",
			"rem",
			"L'acool et les femmes, la vie",
			26,
			1050,
			"Avenue de la Bière 18",
			"Brasserie",
			"biere.de.qualite@alcohol.yes",
			"075/015.10.12",
			false,
			null,
			current_date(),
			"Admin");
            
INSERT INTO Personne (Nom,
					Prenom,
					Pseudo,
					MotDePasse,
					Age,
					AdrNPA,
					AdrRue,
					AdrVille,
					Mail,
					NumTel,
					EstSupprime,
					DateSuppression,
					DateInscription,
					DroitTitre_fk)
	VALUES ("Cequetudis",
			"Mika",
			"mik",
			"Playing games as life method",
			21,
			2342,
			"Gaming Street 6",
			"Gameland",
			"jeux.video@game.pl",
			"012/345.67.89",
			false,
			null,
			current_date(),
			"Admin");
            
INSERT INTO Personne (Nom,
					Prenom,
					Pseudo,
					MotDePasse,
					Age,
					AdrNPA,
					AdrRue,
					AdrVille,
					Mail,
					NumTel,
					EstSupprime,
					DateSuppression,
					DateInscription,
					DroitTitre_fk)
	VALUES ("UnLivre",
			"AuréLie",
			"aur",
			"La lecture, c'est la nature",
			24,
			1997,
			"Chemin de la Poésie 27",
			"Bibilotèque",
			"livres.lecture@biblio.hp",
			"077/793.15.67",
			false,
			null,
			current_date(),
			"Admin");
            
INSERT INTO Personne (Nom,
					Prenom,
					Pseudo,
					MotDePasse,
					Age,
					AdrNPA,
					AdrRue,
					AdrVille,
					Mail,
					NumTel,
					EstSupprime,
					DateSuppression,
					DateInscription,
					DroitTitre_fk)
	VALUES ("RienDuTout",
			"Naddy",
			"nad",
			"Allahu Akbar!... J'ai perdu :(",
			27,
			4256,
			"Rue des Bombes 13",
			"Explosion",
			"bomb.man@allah.go",
			"079/666.42.19",
			false,
			null,
			current_date(),
			"User");
            
INSERT INTO Personne (Nom,
					Prenom,
					Pseudo,
					MotDePasse,
					Age,
					AdrNPA,
					AdrRue,
					AdrVille,
					Mail,
					NumTel,
					EstSupprime,
					DateSuppression,
					DateInscription,
					DroitTitre_fk)
	VALUES ("CestFini",
			"Jimmy",
			"jim",
			"Mon talent et Paint pour surmonter la vie",
			26,
			4223,
			"Avenue du Talent 69",
			"La Baignoire",
			"talent.paint@genius.me",
			"077/777.77.76",
			false,
			null,
			current_date(),
			"User");
            
INSERT INTO Personne (Nom,
					Prenom,
					Pseudo,
					MotDePasse,
					Age,
					AdrNPA,
					AdrRue,
					AdrVille,
					Mail,
					NumTel,
					EstSupprime,
					DateSuppression,
					DateInscription,
					DroitTitre_fk)
	VALUES ("NicoNi",
			"Nico",
			"nic",
			"Yazawa Nico, todokeru heart no NicoNicoNii~",
			25,
			2222,
			"Chemin des Idoles 22",
			"Idoland",
			"nico.nico.nii@nico.ni",
			"022/222.22.21",
			false,
			null,
			current_date(),
			"User");
            
-- ############################################################## MEMBRES ########################################################################

INSERT INTO Membre (
					NbEmpruntEnCours,
					NbReservationEnCours,
					Abonne,
					Id_fk) 
    VALUES 
    (2,
    3,
    true,
    (SELECT Id
    FROM Personne
    WHERE Pseudo = "user") );


INSERT INTO Membre (CarteIdentite,
					NbEmpruntEnCours,
					NbReservationEnCours,
					Abonne,
					Id_fk) 
    VALUES 
    ("Naddy.pdf",
    2,
    3,
    true,
    (SELECT Id
    FROM Personne
    WHERE Nom = "RienDuTout" AND Prenom = "Naddy") );

INSERT INTO Membre (CarteIdentite,
					NbEmpruntEnCours,
					NbReservationEnCours,
					Abonne,
					Id_fk) 
    VALUES 
    ("Jimmy.pdf",
    4,
    2,
    true,
    (SELECT Id
    FROM Personne
    WHERE Nom = "CestFini" AND Prenom = "Jimmy") );

INSERT INTO Membre (CarteIdentite,
					NbEmpruntEnCours,
					NbReservationEnCours,
					Abonne,
					Id_fk) 
    VALUES 
    ("Nico.pdf",
    1,
    3,
    true,
    (SELECT Id
    FROM Personne
    WHERE Nom = "NicoNi" AND Prenom = "Nico") );

-- ############################################################## EMPLOYES #######################################################################

INSERT INTO Employe(Salaire,
					NPA_fk,
                    Id_fk)
	VALUES(7007, (SELECT NPA
					FROM Magasin
                    WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), (SELECT Id
																						FROM Personne
																						WHERE Pseudo = "root"));

INSERT INTO Employe(Salaire,
					NPA_fk,
                    Id_fk)
	VALUES(7007, (SELECT NPA
					FROM Magasin
                    WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), (SELECT Id
																						FROM Personne
																						WHERE Nom = "Cequetudis" AND Prenom = "Mika"));

INSERT INTO Employe(Salaire,
					NPA_fk,
                    Id_fk)
	VALUES(6669, (SELECT NPA
					FROM Magasin
                    WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), (SELECT Id
																						FROM Personne
																						WHERE Nom = "Pas" AND Prenom = "Nathan"));

INSERT INTO Employe(Salaire,
					NPA_fk,
                    Id_fk)
	VALUES(6656, (SELECT NPA
					FROM Magasin
                    WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), (SELECT Id
																						FROM Personne
																						WHERE Nom = "Dofa" AND Prenom = "Rémi"));

INSERT INTO Employe(Salaire,
					NPA_fk,
                    Id_fk)
	VALUES(6027, (SELECT NPA
					FROM Magasin
                    WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), (SELECT Id
																						FROM Personne
																						WHERE Nom = "UnLivre" AND Prenom = "AuréLie"));

-- ############################################################## ETATS ##########################################################################

INSERT INTO Etat (Titre) VALUES ("OK");
INSERT INTO Etat (Titre) VALUES ("Moyen");
INSERT INTO Etat (Titre) VALUES ("Mauvais");

-- ############################################################## EDITEURS ##########################################################################

INSERT INTO Editeur (Nom) VALUES ("2K Games");
INSERT INTO Editeur (Nom) VALUES ("Blizzard Entertainment");
INSERT INTO Editeur (Nom) VALUES ("Capcom");
INSERT INTO Editeur (Nom) VALUES ("FireFly Studios");
INSERT INTO Editeur (Nom) VALUES ("Level-5");
INSERT INTO Editeur (Nom) VALUES ("Microsoft Games");
INSERT INTO Editeur (Nom) VALUES ("Nintendo");
INSERT INTO Editeur (Nom) VALUES ("Rockstar Games");
INSERT INTO Editeur (Nom) VALUES ("Sierra");
INSERT INTO Editeur (Nom) VALUES ("Square Enix");
INSERT INTO Editeur (Nom) VALUES ("Ubisoft");

-- ############################################################## GENRE ##########################################################################

INSERT INTO Genre (Titre) VALUES ("Action");
INSERT INTO Genre (Titre) VALUES ("Aventure");
INSERT INTO Genre (Titre) VALUES ("Combat");
INSERT INTO Genre (Titre) VALUES ("Course");
INSERT INTO Genre (Titre) VALUES ("FPS");
INSERT INTO Genre (Titre) VALUES ("Gore");
INSERT INTO Genre (Titre) VALUES ("Mistère");
INSERT INTO Genre (Titre) VALUES ("Open World");
INSERT INTO Genre (Titre) VALUES ("Plates-formes");
INSERT INTO Genre (Titre) VALUES ("Puzzle");
INSERT INTO Genre (Titre) VALUES ("RPG");
INSERT INTO Genre (Titre) VALUES ("Shooter");
INSERT INTO Genre (Titre) VALUES ("Stratégie");

-- ############################################################## CONSOLES ####################################################################

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("PC", 1980);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("NES", 1983);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("SNES", 1990);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("GameBoy", 1989);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("Nintendo64", 1996);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("GameBoyColor", 1996);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("GameBoyAdvance", 2001);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("GameCube", 2001);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("NintendoDS", 2004);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("Wii", 2006);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("Nintendo3DS", 2011);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("WiiU", 2012);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("PlayStation", 1994);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("PlayStation2", 2000);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("PSP", 2004);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("PlayStation3", 2006);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("PSPVita", 2011);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("PlayStation4", 2013);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("XBOX", 2001);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("XBOX360", 2005);

INSERT INTO Console (Nom,
    AnneeProduction) 
    VALUES ("XBOXOne", 2013);

-- ############################################################## JEUX ##########################################################################

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Stronghold", 2001, 12, "PC", "FireFly Studios", "Stratégie");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Super Mario Bros.", 1985, 3, "NES", "Nintendo", "Plates-formes");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Tetris", 1989, 3, "GameBoy", "Nintendo", "Puzzle");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Super Mario Kart", 1992, 3, "SNES", "Nintendo", "Course");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Super Smash Bros", 1999, 12, "Nintendo64", "Nintendo", "Combat");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Pokemon Or/Argent/Cristal", 2000, 3, "GameBoyColor", "Nintendo", "Aventure");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Fire Emblem: The Sacred Stones", 2004, 7, "GameBoyAdvance", "Nintendo", "RPG");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("The Legend of Zelda: The Wind Waker", 2002, 7, "GameCube", "Nintendo", "Aventure");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Resident Evil 4", 2007, 18, "Wii", "Capcom", "Gore");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Professeur Layton et l'Etrange Village", 2007, 7, "NintendoDS", "Level-5", "Mistère");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Bravely Default", 2012, 12, "Nintendo3DS", "Square Enix", "RPG");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Splatoon", 2015, 7, "WiiU", "Nintendo", "Shooter");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Street Fighter Collection", 1997, 16, "PlayStation", "Capcom", "Combat");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Half-Life", 2001, 16, "PlayStation2", "Sierra", "FPS");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Assassin's Creed: Bloodlines", 2009, 16, "PSP", "Ubisoft", "Action");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Final Fantasy XIII", 2009, 16, "PlayStation3", "Square Enix", "RPG");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Borderlands 2", 2012, 18, "PSPVita", "2K Games", "Action");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Overwatch", 2016, 16, "PlayStation4", "Blizzard Entertainment", "FPS");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Fable", 2004, 16, "XBOX", "Microsoft Games", "RPG");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Grand Theft Auto V", 2013, 18, "XBOX360", "Rockstar Games", "Open World");

INSERT INTO Jeu (Titre,
				AnneeProduction,
				LimiteAge,    
				NomConsole_fk,
				NomEditeur_fk,
				GenreTitre_fk) 
				VALUES ("Watch Dogs", 2014, 18, "XBOXOne", "Ubisoft", "Action");

-- ############################################################## EXEMPLAIRES JEUX ##################################################################

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PC", "Stronghold");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PC", "Stronghold");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PC", "Stronghold");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (4, "Quelques erreurs", "Moyen", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PC", "Stronghold");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (5, "Ne marche pas du tout", "Mauvais", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PC", "Stronghold");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "NES", "Super Mario Bros.");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "NES", "Super Mario Bros.");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "NES", "Super Mario Bros.");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Quelques erreurs", "Moyen", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoy", "Tetris");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Bon état", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoy", "Tetris");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Fonctionne bien", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoy", "Tetris");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (4, "Bon fonctionnement", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoy", "Tetris");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "SNES", "Super Mario Kart");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "SNES", "Super Mario Kart");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "SNES", "Super Mario Kart");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
									FROM Magasin
									WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation", "Street Fighter Collection");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Ne marche pas du tout", "Mauvais", (SELECT NPA
									FROM Magasin
									WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation", "Street Fighter Collection");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Fonctionne bien", "OK", (SELECT NPA
									FROM Magasin
									WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation", "Street Fighter Collection");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (4, "Bon fonctionnement", "OK", (SELECT NPA
									FROM Magasin
									WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation", "Street Fighter Collection");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
									FROM Magasin
									WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoyColor", "Pokemon Or/Argent/Cristal");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
									FROM Magasin
									WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoyColor", "Pokemon Or/Argent/Cristal");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
									FROM Magasin
									WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoyColor", "Pokemon Or/Argent/Cristal");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Nintendo64", "Super Smash Bros");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Nintendo64", "Super Smash Bros");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Quelques erreurs", "Moyen", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Nintendo64", "Super Smash Bros");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (4, "Bon fonctionnement", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Nintendo64", "Super Smash Bros");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation2", "Half-Life");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation2", "Half-Life");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation2", "Half-Life");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
							FROM Magasin
							WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameCube", "The Legend of Zelda: The Wind Waker");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
							FROM Magasin
							WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameCube", "The Legend of Zelda: The Wind Waker");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
							FROM Magasin
							WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameCube", "The Legend of Zelda: The Wind Waker");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
							FROM Magasin
							WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoyAdvance", "Fire Emblem: The Sacred Stones");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
							FROM Magasin
							WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoyAdvance", "Fire Emblem: The Sacred Stones");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
							FROM Magasin
							WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoyAdvance", "Fire Emblem: The Sacred Stones");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Ne marche pas du tout", "Mauvais", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOX", "Fable");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Bon état", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOX", "Fable");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Fonctionne bien", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOX", "Fable");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (4, "Bon fonctionnement", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOX", "Fable");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
						FROM Magasin
						WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "NintendoDS", "Professeur Layton et l'Etrange Village");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
						FROM Magasin
						WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "NintendoDS", "Professeur Layton et l'Etrange Village");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
						FROM Magasin
						WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "NintendoDS", "Professeur Layton et l'Etrange Village");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
										FROM Magasin
										WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PSP", "Assassin's Creed: Bloodlines");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Quelques erreurs", "Moyen", (SELECT NPA
										FROM Magasin
										WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PSP", "Assassin's Creed: Bloodlines");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Fonctionne bien", "OK", (SELECT NPA
										FROM Magasin
										WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PSP", "Assassin's Creed: Bloodlines");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (4, "Bon fonctionnement", "OK", (SELECT NPA
										FROM Magasin
										WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PSP", "Assassin's Creed: Bloodlines");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Wii", "Resident Evil 4");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Wii", "Resident Evil 4");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Wii", "Resident Evil 4");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
											FROM Magasin
											WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation3", "Final Fantasy XIII");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
											FROM Magasin
											WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation3", "Final Fantasy XIII");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
											FROM Magasin
											WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation3", "Final Fantasy XIII");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PSPVita", "Borderlands 2");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PSPVita", "Borderlands 2");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PSPVita", "Borderlands 2");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (4, "Quelques erreurs", "Moyen", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PSPVita", "Borderlands 2");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOX360", "Grand Theft Auto V");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOX360", "Grand Theft Auto V");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOX360", "Grand Theft Auto V");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (4, "Ne marche pas du tout", "Mauvais", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOX360", "Grand Theft Auto V");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Nintendo3DS", "Bravely Default");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Nintendo3DS", "Bravely Default");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Nintendo3DS", "Bravely Default");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOXOne", "Watch Dogs");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOXOne", "Watch Dogs");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Ne marche pas du tout", "Mauvais", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOXOne", "Watch Dogs");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (4, "Bon fonctionnement", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOXOne", "Watch Dogs");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "WiiU", "Splatoon");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "WiiU", "Splatoon");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "WiiU", "Splatoon");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation4", "Overwatch");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation4", "Overwatch");

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
												FROM Magasin
												WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation4", "Overwatch");

-- ############################################################## EXEMPLAIRES CONSOLES ##############################################################

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "NES", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "NES", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
																FROM Magasin
																WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "NES", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "SNES", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "SNES", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
																FROM Magasin
																WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "SNES", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoy", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoy", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
																FROM Magasin
																WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoy", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Nintendo64", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Nintendo64", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Nintendo64", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoyColor", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoyColor", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoyColor", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoyAdvance", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
														FROM Magasin
														WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoyAdvance", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
														FROM Magasin
														WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameBoyAdvance", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameCube", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameCube", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
																FROM Magasin
																WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "GameCube", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "NintendoDS", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "NintendoDS", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "NintendoDS", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Wii", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Wii", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
																FROM Magasin
																WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Wii", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Nintendo3DS", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Nintendo3DS", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "Nintendo3DS", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "WiiU", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "WiiU", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
																FROM Magasin
																WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "WiiU", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation2", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation2", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation2", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PSP", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PSP", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
																FROM Magasin
																WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PSP", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation3", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation3", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation3", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PSPVita", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PSPVita", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
																FROM Magasin
																WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PSPVita", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation4", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
														FROM Magasin
														WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation4", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
														FROM Magasin
														WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "PlayStation4", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOX", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOX", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
																FROM Magasin
																WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOX", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOX360", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOX360", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
																FROM Magasin
																WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOX360", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (1, "Bon état", "OK", (SELECT NPA
													FROM Magasin
													WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOXOne", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (2, "Fonctionne bien", "OK", (SELECT NPA
															FROM Magasin
															WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOXOne", null);

INSERT INTO Exemplaire (NumExemplaire,
						EtatCommentaire,
						TitreEtat_fk,
						NPA_fk,
						NomConsole_fk,
						NomJeu_fk) 
						VALUES (3, "Bon fonctionnement", "OK", (SELECT NPA
																FROM Magasin
																WHERE Ville = "Lausanne" AND Rue = "Rue de Genève" AND NumRue = 20), "XBOXOne", null);

-- ############################################################## RESERVATIONS #######################################################################

INSERT INTO Reservation (DateReservation,
						DateRetraitSouhaite,
						IdMembre_fk,
						IdExemplaire_fk,
						NumExemplaire_fk)
						VALUES (current_date(), DATE_ADD(DateReservation,INTERVAL 10 DAY),
								(SELECT Id
								FROM Personne
								WHERE Nom = "RienDuTout" AND Prenom = "Naddy"),
								(SELECT Id
								FROM Exemplaire
								WHERE NomJeu_fk = "Pokemon Or/Argent/Cristal" AND NumExemplaire = 1), 1);

INSERT INTO Reservation (DateReservation,
						DateRetraitSouhaite,
						IdMembre_fk,
						IdExemplaire_fk,
						NumExemplaire_fk)
						VALUES (current_date(), DATE_ADD(DateReservation,INTERVAL 10 DAY),
								(SELECT Id
								FROM Personne
								WHERE Nom = "RienDuTout" AND Prenom = "Naddy"),
								(SELECT Id
								FROM Exemplaire
								WHERE NomJeu_fk = "Fire Emblem: The Sacred Stones" AND NumExemplaire = 1), 1);

INSERT INTO Reservation (DateReservation,
						DateRetraitSouhaite,
						IdMembre_fk,
						IdExemplaire_fk,
						NumExemplaire_fk)
						VALUES (current_date(), DATE_ADD(DateReservation,INTERVAL 10 DAY),
								(SELECT Id
								FROM Personne
								WHERE Nom = "RienDuTout" AND Prenom = "Naddy"),
								(SELECT Id
								FROM Exemplaire
								WHERE NomJeu_fk = "Bravely Default" AND NumExemplaire = 1), 1);

INSERT INTO Reservation (DateReservation,
						DateRetraitSouhaite,
						IdMembre_fk,
						IdExemplaire_fk,
						NumExemplaire_fk)
						VALUES (current_date(), DATE_ADD(DateReservation,INTERVAL 10 DAY),
								(SELECT Id
								FROM Personne
								WHERE Nom = "CestFini" AND Prenom = "Jimmy"),
								(SELECT Id
								FROM Exemplaire
								WHERE NomJeu_fk = "Overwatch" AND NumExemplaire = 1), 1);

INSERT INTO Reservation (DateReservation,
						DateRetraitSouhaite,
						IdMembre_fk,
						IdExemplaire_fk,
						NumExemplaire_fk)
						VALUES (current_date(), DATE_ADD(DateReservation,INTERVAL 10 DAY),
								(SELECT Id
								FROM Personne
								WHERE Nom = "CestFini" AND Prenom = "Jimmy"),
								(SELECT Id
								FROM Exemplaire
								WHERE NomJeu_fk = "Super Mario Kart" AND NumExemplaire = 1), 1);

INSERT INTO Reservation (DateReservation,
						DateRetraitSouhaite,
						IdMembre_fk,
						IdExemplaire_fk,
						NumExemplaire_fk)
						VALUES (current_date(), DATE_ADD(DateReservation,INTERVAL 10 DAY),
								(SELECT Id
								FROM Personne
								WHERE Nom = "NicoNi" AND Prenom = "Nico"),
								(SELECT Id
								FROM Exemplaire
								WHERE NomJeu_fk = "Overwatch" AND NumExemplaire = 2), 2);

INSERT INTO Reservation (DateReservation,
						DateRetraitSouhaite,
						IdMembre_fk,
						IdExemplaire_fk,
						NumExemplaire_fk)
						VALUES (current_date(), DATE_ADD(DateReservation,INTERVAL 10 DAY),
								(SELECT Id
								FROM Personne
								WHERE Nom = "NicoNi" AND Prenom = "Nico"),
								(SELECT Id
								FROM Exemplaire
								WHERE NomJeu_fk = "Watch Dogs" AND NumExemplaire = 1), 1);

INSERT INTO Reservation (DateReservation,
						DateRetraitSouhaite,
						IdMembre_fk,
						IdExemplaire_fk,
						NumExemplaire_fk)
						VALUES (current_date(), DATE_ADD(DateReservation,INTERVAL 10 DAY),
								(SELECT Id
								FROM Personne
								WHERE Nom = "NicoNi" AND Prenom = "Nico"),
								(SELECT Id
								FROM Exemplaire
								WHERE NomJeu_fk = "Tetris" AND NumExemplaire = 1), 1);

-- ############################################################## EMPRUNTS ##########################################################################

INSERT INTO Emprunt (DateEmprunt,
					DureeJour,
					NombreProlongation,
					IdMembre_fk,
					IdExemplaire_fk,
					NumExemplaire_fk)
					VALUES (current_date(), 10, 0,
							(SELECT Id
							FROM Personne
							WHERE Nom = "RienDuTout" AND Prenom = "Naddy"),
							(SELECT Id
							FROM Exemplaire
							WHERE NomJeu_fk = "Final Fantasy XIII" AND NumExemplaire = 1), 1);

INSERT INTO Emprunt (DateEmprunt,
					DureeJour,
					NombreProlongation,
					IdMembre_fk,
					IdExemplaire_fk,
					NumExemplaire_fk)
					VALUES (current_date(), 10, 0,
							(SELECT Id
							FROM Personne
							WHERE Nom = "RienDuTout" AND Prenom = "Naddy"),
							(SELECT Id
							FROM Exemplaire
							WHERE NomJeu_fk = "The Legend of Zelda: The Wind Waker" AND NumExemplaire = 1), 1);

INSERT INTO Emprunt (DateEmprunt,
					DureeJour,
					NombreProlongation,
					IdMembre_fk,
					IdExemplaire_fk,
					NumExemplaire_fk)
					VALUES (current_date(), 10, 0,
							(SELECT Id
							FROM Personne
							WHERE Nom = "CestFini" AND Prenom = "Jimmy"),
							(SELECT Id
							FROM Exemplaire
							WHERE NomJeu_fk = "Final Fantasy XIII" AND NumExemplaire = 2), 2);

INSERT INTO Emprunt (DateEmprunt,
					DureeJour,
					NombreProlongation,
					IdMembre_fk,
					IdExemplaire_fk,
					NumExemplaire_fk)
					VALUES (current_date(), 10, 0,
							(SELECT Id
							FROM Personne
							WHERE Nom = "CestFini" AND Prenom = "Jimmy"),
							(SELECT Id
							FROM Exemplaire
							WHERE NomJeu_fk = "Pokemon Or/Argent/Cristal" AND NumExemplaire = 2), 2);

INSERT INTO Emprunt (DateEmprunt,
					DureeJour,
					NombreProlongation,
					IdMembre_fk,
					IdExemplaire_fk,
					NumExemplaire_fk)
					VALUES ('2016-12-10', 10, 0,
							(SELECT Id
							FROM Personne
							WHERE Nom = "CestFini" AND Prenom = "Jimmy"),
							(SELECT Id
							FROM Exemplaire
							WHERE NomJeu_fk = "Splatoon" AND NumExemplaire = 1), 1);

INSERT INTO Emprunt (DateEmprunt,
					DureeJour,
					NombreProlongation,
					IdMembre_fk,
					IdExemplaire_fk,
					NumExemplaire_fk)
					VALUES (current_date(), 10, 0,
							(SELECT Id
							FROM Personne
							WHERE Nom = "NicoNi" AND Prenom = "Nico"),
							(SELECT Id
							FROM Exemplaire
							WHERE NomJeu_fk = "Final Fantasy XIII" AND NumExemplaire = 3), 3);

############################################################## AVIS ##########################################################################

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(8, "Ce jeu est incroyable, très recommendable.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "PC"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(9, "C'est un très bon jeu de plateformes, j'ai adoré.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "NES"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(7, "Bon jeu, mais au bout d'un temps c'est la même chose.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "GameBoy"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(8, "Tres bien, surtout pour jouer à plusieurs.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "SNES"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(9, "Incroyable, combatre avec ces persos et une bonne ambiance solo et multijouer.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "Nintendo64"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(10, "Un des meilleurs jeu que j'ai joué, histoire très bonne et avec possibilité de jouer à plusieurs.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "GameBoyColor"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(7, "Il faut bien savoir faire différentes tactiques pour passer le jeu, mais très bon jeu.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "GameBoyAdvance"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(9, "Un des meilleurs Zelda que j'ai joué avec Twilight Princess, une histoire vraiment bonne.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "GameCube"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(7, "Tres sympa comme jeu, même si c'est un peu frustrant quand tu loupes un enigme xD",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "NintendoDS"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(9, "Très cool, j'ai eu vachement peur quand je jouais la nuit, je croiyais vraiment qu'un zombie allait apparître...",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "Wii"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(10, "Jeu surprenent, je m'attendais pas du tout à qu'il soit aussi génial; en plus, il y a les voix en japonais :D",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "Nintendo3DS"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(8, "Il est hyper bien, c'est cool de jouer par internet avec puis tu trouves facile des gens pour jouer.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "WiiU"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(8, "Ce jeu de combat est des meilleurs que j'ai pu jouer, il est trop bien.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "PlayStation"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(9, "Ce jeu est vraiment incroyable, même avec ces graphiques, l'histoire est trop bien, il faut y jouer.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "PlayStation2"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(7, "Le jeu est bien, c'est cool de pouvoir incarner un assassin de la vielle époque.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "PSP"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(9, "Quoi dire de plus d'un FF? Toujours aussi incroyable et avec une très bonne histoire.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "PlayStation3"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(9, "Ce jeu est trop bien, j'ai adoré le style de jeu, en plus le but c'est de tuer plein de gens... xD",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "PSPVita"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(8, "Jouer à ce jeu en multijouer est la meilleure façon de profiter du jeu.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "PlayStation4"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(6, "Ce jeu est cool, j'ai bien aimàe l'histoire qu'il y a derrière.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "XBOX"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(6, "Ça change pas beaucoup des autres GTA, mais il est bien, moi j'ai bien aimé.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "XBOX360"));

INSERT INTO Avis (Note,
				Commentaire,
				JeuTitre_fk) 
				VALUES 
				(8, "Vraiment trop bien comme jeu d'infiltration, je vous le recommende bien.",
                (SELECT Titre
				FROM Jeu
				WHERE NomConsole_fk = "XBOXOne"));

-- ############################################################## TYPEAMENDES #######################################################################

INSERT INTO TypeAmende (Nom,
						PrixJournee,
						MontantUnique)
						VALUES ("Retard", 5, 0);

INSERT INTO TypeAmende (Nom,
						PrixJournee,
						MontantUnique)
						VALUES ("Abime", 10, 0);

INSERT INTO TypeAmende (Nom,
						PrixJournee,
						MontantUnique)
						VALUES ("JeuPerdu", 0, 50);

INSERT INTO TypeAmende (Nom,
						PrixJournee,
						MontantUnique)
						VALUES ("ConsolePerdue", 0, 200);

-- ############################################################## AMENDES #######################################################################
-- Verification des vues, pas une insertion correcte
-- insertion manuel d'amende
-- INSERT INTO Amende (DateInitial,
-- 					TypeAmende_fk,
-- 					EmpruntId_fk)
--                     VALUES ('2016-12-10', "Retard", 5);

-- insertion automatiques des amendes
CALL MAJ_amendes();

-- ############################################################## TESTS ##########################################################################

SELECT * FROM Personne;
SELECT * FROM Employe;
SELECT * FROM Membre;
SELECT * FROM Editeur;
SELECT * FROM Genre;
SELECT * FROM Console;
SELECT * FROM Jeu;
SELECT * FROM Exemplaire;
SELECT * FROM Etat;
SELECT * FROM Reservation;
SELECT * FROM Emprunt;
SELECT * FROM Avis;
SELECT * FROM Amende;
